-- MySQL dump 10.13  Distrib 8.0.33, for Win64 (x86_64)
--
-- Host: localhost    Database: test
-- ------------------------------------------------------
-- Server version	8.0.33
create database Happy_Progamming;
use Happy_Progamming;
/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!50503 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `cvmentor`
--

DROP TABLE IF EXISTS `cvmentor`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `cvmentor` (
  `cv_id` int NOT NULL AUTO_INCREMENT,
  `mentor_id` int DEFAULT NULL,
  `full_name` varchar(100) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NOT NULL,
  `dob` date NOT NULL,
  `email` varchar(50) NOT NULL,
  `sex` bit(1) DEFAULT NULL,
  `phone` varchar(10) DEFAULT NULL,
  `address` varchar(50) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci DEFAULT NULL,
  `profession` varchar(50) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci DEFAULT NULL,
  `intro_job` nvarchar(1500) NULL,
  `avatar` varchar(200) DEFAULT NULL,
  `Service` nvarchar(1500) NULL,
  `Achievement` nvarchar(1500) NULL,
  PRIMARY KEY (`cv_id`),
  KEY `mentor_id` (`mentor_id`),
  CONSTRAINT `cvmentor_ibfk_1` FOREIGN KEY (`mentor_id`) REFERENCES `mentor` (`mentor_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `cvmentor`
--

LOCK TABLES `cvmentor` WRITE;
/*!40000 ALTER TABLE `cvmentor` DISABLE KEYS */;
INSERT INTO `happy_progamming`.`cvmentor` (`cv_id`, `mentor_id`, `full_name`, `dob`, `email`, `sex`, `phone`, `address`, `profession`, `intro_job`, `avatar`) VALUES ('1', '2', 'Maria Ozawa', '2002-01-25', 'hoangtuan69a1@gmail.com', '1', '0978300936', 'Hà Nội', 'Sinh viên', 'chuyen kèm riêng các em từ 18 -20', 'https://i.pinimg.com/originals/e0/0e/d9/e00ed96cc83f6dc8d3b54e9fb63161d2.jpg');
/*!40000 ALTER TABLE `cvmentor` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `mentor`
--

DROP TABLE IF EXISTS `mentor`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `mentor` (
  `mentor_id` int NOT NULL AUTO_INCREMENT,
  `user_id` int DEFAULT NULL,
  `status_id` int DEFAULT NULL,
  PRIMARY KEY (`mentor_id`),
  KEY `user_id` (`user_id`),
  KEY `status_id` (`status_id`),
  CONSTRAINT `mentor_ibfk_1` FOREIGN KEY (`user_id`) REFERENCES `users` (`user_id`),
  CONSTRAINT `mentor_ibfk_2` FOREIGN KEY (`status_id`) REFERENCES `mentorstatus` (`status_id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `mentor`
--

LOCK TABLES `mentor` WRITE;
/*!40000 ALTER TABLE `mentor` DISABLE KEYS */;
INSERT INTO `mentor` VALUES (1,1,1),(2,2,1);
/*!40000 ALTER TABLE `mentor` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `mentorskill`
--

DROP TABLE IF EXISTS `mentorskill`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `mentorskill` (
  `mentor_id` int DEFAULT NULL,
  `Skill_id` int DEFAULT NULL,
  KEY `Skill_id` (`Skill_id`),
  KEY `mentor_id` (`mentor_id`),
  CONSTRAINT `mentorskill_ibfk_1` FOREIGN KEY (`Skill_id`) REFERENCES `skill` (`skill_id`),
  CONSTRAINT `mentorskill_ibfk_2` FOREIGN KEY (`mentor_id`) REFERENCES `mentor` (`mentor_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `mentorskill`
--

LOCK TABLES `mentorskill` WRITE;
/*!40000 ALTER TABLE `mentorskill` DISABLE KEYS */;
INSERT INTO `mentorskill` VALUES (2,1),(2,3),(2,4),(1,3),(1,4);
/*!40000 ALTER TABLE `mentorskill` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `mentorstatus`
--

DROP TABLE IF EXISTS `mentorstatus`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `mentorstatus` (
  `status_id` int NOT NULL,
  `status_name` varchar(20) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NOT NULL,
  PRIMARY KEY (`status_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `mentorstatus`
--

LOCK TABLES `mentorstatus` WRITE;
/*!40000 ALTER TABLE `mentorstatus` DISABLE KEYS */;
INSERT INTO `mentorstatus` VALUES (1,'Active'),(2,'Inactive'),(3,'Lock');
/*!40000 ALTER TABLE `mentorstatus` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `payment`
--

DROP TABLE IF EXISTS `payment`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `payment` (
  `payment_id` int NOT NULL AUTO_INCREMENT,
  `request_id` int DEFAULT NULL,
  `payment_date` date DEFAULT NULL,
  `amount` int DEFAULT NULL,
  PRIMARY KEY (`payment_id`),
  KEY `request_id` (`request_id`),
  CONSTRAINT `payment_ibfk_1` FOREIGN KEY (`request_id`) REFERENCES `requests` (`request_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `payment`
--

LOCK TABLES `payment` WRITE;
/*!40000 ALTER TABLE `payment` DISABLE KEYS */;
/*!40000 ALTER TABLE `payment` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `profile`
--

DROP TABLE IF EXISTS `profile`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `profile` (
  `profile_id` int NOT NULL AUTO_INCREMENT,
  `user_id` int DEFAULT NULL,
  `avatar` varchar(200) NOT NULL,
  `full_name` varchar(50) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NOT NULL,
  `sex` bit(1) DEFAULT NULL,
  `phone` varchar(10) DEFAULT NULL,
  `dob` date DEFAULT NULL,
  `address` varchar(50) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci DEFAULT NULL,
  `description` varchar(1500) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci DEFAULT NULL,
  PRIMARY KEY (`profile_id`),
  KEY `user_id` (`user_id`),
  CONSTRAINT `profile_ibfk_1` FOREIGN KEY (`user_id`) REFERENCES `users` (`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `profile`
--

LOCK TABLES `profile` WRITE;
INSERT INTO `happy_progamming`.`profile` (`profile_id`, `user_id`, `avatar`, `full_name`, `sex`, `phone`, `dob`, `address`, `description`) VALUES ('1', '2', 'https://photo-cms-giaoducthoidai.epicdn.me/w820/Uploaded/2023/sfrss/2015_07_30/ab9d064ebfc259af11405afc7cbb023b_FZQT.jpg', 'Maria Ozawa', b'1', '0978300936', '1990-01-01', 'Japan', 'do you know me ?');
/*!40000 ALTER TABLE `profile` DISABLE KEYS */;
/*!40000 ALTER TABLE `profile` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `request_skill`
--

DROP TABLE IF EXISTS `request_skill`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `request_skill` (
  `request_id` int NOT NULL,
  `skill_id` int NOT NULL,
  PRIMARY KEY (`request_id`,`skill_id`),
  CONSTRAINT `request_id` FOREIGN KEY (`request_id`) REFERENCES `requests` (`request_id`),
  CONSTRAINT `skill_id` FOREIGN KEY (`request_id`) REFERENCES `skill` (`skill_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `request_skill`
--

LOCK TABLES `request_skill` WRITE;
/*!40000 ALTER TABLE `request_skill` DISABLE KEYS */;
INSERT INTO `request_skill` VALUES (2,4);
/*!40000 ALTER TABLE `request_skill` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `requestdetail`
--

DROP TABLE IF EXISTS `requestdetail`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `requestdetail` (
  `request_id` int DEFAULT NULL,
  `skill_id` int DEFAULT NULL,
  KEY `skill_id` (`skill_id`),
  KEY `request_id` (`request_id`),
  CONSTRAINT `requestdetail_ibfk_1` FOREIGN KEY (`skill_id`) REFERENCES `skill` (`skill_id`),
  CONSTRAINT `requestdetail_ibfk_2` FOREIGN KEY (`request_id`) REFERENCES `requests` (`request_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `requestdetail`
--

LOCK TABLES `requestdetail` WRITE;
/*!40000 ALTER TABLE `requestdetail` DISABLE KEYS */;
/*!40000 ALTER TABLE `requestdetail` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `requestmentor`
--

DROP TABLE IF EXISTS `requestmentor`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `requestmentor` (
  `request_id` int DEFAULT NULL,
  `mentor_id` int DEFAULT NULL,
  KEY `request_id` (`request_id`),
  KEY `mentor_id` (`mentor_id`),
  CONSTRAINT `requestmentor_ibfk_1` FOREIGN KEY (`request_id`) REFERENCES `requests` (`request_id`),
  CONSTRAINT `requestmentor_ibfk_2` FOREIGN KEY (`mentor_id`) REFERENCES `mentor` (`mentor_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `requestmentor`
--

LOCK TABLES `requestmentor` WRITE;
/*!40000 ALTER TABLE `requestmentor` DISABLE KEYS */;
INSERT INTO `requestmentor` VALUES (1,1),(2,2);
/*!40000 ALTER TABLE `requestmentor` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `requests`
--

DROP TABLE IF EXISTS `requests`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `requests` (
  `request_id` int NOT NULL AUTO_INCREMENT,
  `user_id` int DEFAULT NULL,
  `title` text,
  `deadline_date` date DEFAULT NULL,
  `deadline_hour` double DEFAULT NULL,
  `create_date` date DEFAULT NULL,
  `content` text,
  `status_id` int DEFAULT NULL,
  PRIMARY KEY (`request_id`),
  KEY `user_id` (`user_id`),
  KEY `status_id` (`status_id`),
  CONSTRAINT `requests_ibfk_1` FOREIGN KEY (`user_id`) REFERENCES `users` (`user_id`),
  CONSTRAINT `requests_ibfk_2` FOREIGN KEY (`status_id`) REFERENCES `requeststatus` (`status_id`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `requests`
--

LOCK TABLES `requests` WRITE;
/*!40000 ALTER TABLE `requests` DISABLE KEYS */;
INSERT INTO `requests` VALUES (1,4,'helo','2023-12-02',8.5,'2023-06-12','Help me',1),(2,4,'fd','2023-06-26',4,'2023-06-13','df',1);
/*!40000 ALTER TABLE `requests` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `requeststatus`
--

DROP TABLE IF EXISTS `requeststatus`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `requeststatus` (
  `status_id` int NOT NULL,
  `status_name` varchar(50) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci DEFAULT NULL,
  PRIMARY KEY (`status_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `requeststatus`
--

LOCK TABLES `requeststatus` WRITE;
/*!40000 ALTER TABLE `requeststatus` DISABLE KEYS */;
INSERT INTO `requeststatus` VALUES (1,'Open'),(2,'Processing'),(3,'Cancel'),(4,'Closed');
/*!40000 ALTER TABLE `requeststatus` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `reviews`
--

DROP TABLE IF EXISTS `reviews`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `reviews` (
  `review_id` int NOT NULL AUTO_INCREMENT,
  `user_id` int DEFAULT NULL,
  `rating` int DEFAULT NULL,
  `comment_review` varchar(255) DEFAULT NULL,
  `date_comment` date DEFAULT NULL,
  PRIMARY KEY (`review_id`),
  KEY `user_id` (`user_id`),
  CONSTRAINT `reviews_ibfk_1` FOREIGN KEY (`user_id`) REFERENCES `users` (`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `reviews`
--

LOCK TABLES `reviews` WRITE;
/*!40000 ALTER TABLE `reviews` DISABLE KEYS */;
/*!40000 ALTER TABLE `reviews` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `roles`
--

DROP TABLE IF EXISTS `roles`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `roles` (
  `role_id` int NOT NULL AUTO_INCREMENT,
  `role_name` varchar(50) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NOT NULL,
  PRIMARY KEY (`role_id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `roles`
--

LOCK TABLES `roles` WRITE;
/*!40000 ALTER TABLE `roles` DISABLE KEYS */;
INSERT INTO `roles` VALUES (1,'admin'),(2,'mentor'),(3,'mentee');
/*!40000 ALTER TABLE `roles` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `skill`
--

DROP TABLE IF EXISTS `skill`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `skill` (
  `skill_id` int NOT NULL AUTO_INCREMENT,
  `skill_name` varchar(50) NOT NULL,
  `decription` varchar(1000) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci DEFAULT NULL,
  `skill_price` double NOT NULL,
  `image` varchar(255) NOT NULL,
  `status_skill` varchar(255) NOT NULL,
  PRIMARY KEY (`skill_id`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `skill`
--

LOCK TABLES `skill` WRITE;
/*!40000 ALTER TABLE `skill` DISABLE KEYS */;
INSERT INTO `skill` VALUES (1,'Lập trình C','Kỹ năng lập trình ngôn ngữ C',50,'https://upload.wikimedia.org/wikipedia/commons/thumb/1/18/ISO_C%2B%2B_Logo.svg/150px-ISO_C%2B%2B_Logo.svg.png','active'),(2,'Lập trình Python','Kỹ năng lập trình ngôn ngữ Python',60,'https://images.careerbuilder.vn/content/images/ngon-ngu-lap-trinh-python-CareerBuilder-8.png','active');
/*!40000 ALTER TABLE `skill` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `famework`
-- 
DROP TABLE IF EXISTS `famework`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `famework` (
  `fame_id` int NOT NULL AUTO_INCREMENT,
  `fame_name` varchar(50) NOT NULL,
PRIMARY KEY (`fame_id`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;


--
-- Table structure for table `mentorfamework`
--
DROP TABLE IF EXISTS `mentorfamework`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `mentorfamework` (
  `mentor_id` int DEFAULT NULL,
  `fame_id` int DEFAULT NULL,
  KEY `fame_id` (`fame_id`),
  KEY `mentor_id` (`mentor_id`),
  CONSTRAINT `mentorfamework_ibfk_1` FOREIGN KEY (`fame_id`) REFERENCES `famework` (`fame_id`),
  CONSTRAINT `mentorfamework_ibfk_2` FOREIGN KEY (`mentor_id`) REFERENCES `mentor` (`mentor_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;
--
-- Table structure for table `users`
--

DROP TABLE IF EXISTS `users`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `users` (
  `user_id` int NOT NULL AUTO_INCREMENT,
  `user_name` varchar(100) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NOT NULL,
  `password_user` varchar(50) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NOT NULL,
  `email` varchar(50) NOT NULL,
  `role_id` int DEFAULT NULL,
  PRIMARY KEY (`user_id`),
  UNIQUE KEY `user_name` (`user_name`),
  UNIQUE KEY `email` (`email`),
  KEY `role_id` (`role_id`),
  CONSTRAINT `users_ibfk_1` FOREIGN KEY (`role_id`) REFERENCES `roles` (`role_id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `users`
--

LOCK TABLES `users` WRITE;
/*!40000 ALTER TABLE `users` DISABLE KEYS */;
INSERT INTO `users` VALUES (1,'ngoclong','123','long@gmail.com',2),(2,'vien','123','vien@gmail.com',2),(3,'quan54321','123','quandeptrai@gmail.com',1),(4,'hieu','123','hieu@gmail.com',3);
/*!40000 ALTER TABLE `users` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

INSERT INTO `skill` VALUES (11,'Lập trình C','Kỹ năng lập trình ngôn ngữ C',50,'https://upload.wikimedia.org/wikipedia/commons/thumb/1/18/ISO_C%2B%2B_Logo.svg/150px-ISO_C%2B%2B_Logo.svg.png','active'),
(12,'Lập trình Python','Kỹ năng lập trình ngôn ngữ Python',60,'https://upload.wikimedia.org/wikipedia/commons/thumb/1/18/ISO_C%2B%2B_Logo.svg/150px-ISO_C%2B%2B_Logo.svg.png','active'),
(3,'Lập trình Java','Kỹ năng lập trình ngôn ngữ Java',70,'https://upload.wikimedia.org/wikipedia/commons/thumb/1/18/ISO_C%2B%2B_Logo.svg/150px-ISO_C%2B%2B_Logo.svg.png','active'),
(4,'Lập trình PHP','Kỹ năng lập trình ngôn ngữ PHP',55,'https://upload.wikimedia.org/wikipedia/commons/thumb/1/18/ISO_C%2B%2B_Logo.svg/150px-ISO_C%2B%2B_Logo.svg.png','active'),
(5,'Lập trình Ruby','Kỹ năng lập trình ngôn ngữ Ruby',65,'https://upload.wikimedia.org/wikipedia/commons/thumb/1/18/ISO_C%2B%2B_Logo.svg/150px-ISO_C%2B%2B_Logo.svg.png','active'),
(6,'Lập trình JavaScript','Kỹ năng lập trình ngôn ngữ JavaScript',70,'https://upload.wikimedia.org/wikipedia/commons/thumb/1/18/ISO_C%2B%2B_Logo.svg/150px-ISO_C%2B%2B_Logo.svg.png','active'),
(7,'Lập trình Swift','Kỹ năng lập trình ngôn ngữ Swift',75,'https://upload.wikimedia.org/wikipedia/commons/thumb/1/18/ISO_C%2B%2B_Logo.svg/150px-ISO_C%2B%2B_Logo.svg.png','active'),
(8,'Lập trình Kotlin','Kỹ năng lập trình ngôn ngữ Kotlin',80,'https://upload.wikimedia.org/wikipedia/commons/thumb/1/18/ISO_C%2B%2B_Logo.svg/150px-ISO_C%2B%2B_Logo.svg.png','active'),
(9,'Lập trình C++','Kỹ năng lập trình ngôn ngữ C++',65,'https://upload.wikimedia.org/wikipedia/commons/thumb/1/18/ISO_C%2B%2B_Logo.svg/150px-ISO_C%2B%2B_Logo.svg.png','active'),
(10,'Lập trình C#','Kỹ năng lập trình ngôn ngữ C#',70,'https://upload.wikimedia.org/wikipedia/commons/thumb/1/18/ISO_C%2B%2B_Logo.svg/150px-ISO_C%2B%2B_Logo.svg.png','active');
--
INSERT INTO `famework` (`fame_id`, `fame_name`) VALUES ('1', 'VueJS');
INSERT INTO `famework` (`fame_id`, `fame_name`) VALUES ('2', 'Angular.JS');
INSERT INTO `famework` (`fame_id`, `fame_name`) VALUES ('3', 'Laravel');
INSERT INTO `famework` (`fame_id`, `fame_name`) VALUES ('4', 'ASP.NET');
INSERT INTO `famework` (`fame_id`, `fame_name`) VALUES ('5', 'Spring Boot');
INSERT INTO `famework` (`fame_id`, `fame_name`) VALUES ('6', 'NodeJs');
INSERT INTO `famework` (`fame_id`, `fame_name`) VALUES ('7', 'Ruby on Rails');



-- Dump completed on 2023-06-13 21:47:14
