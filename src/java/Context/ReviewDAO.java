/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package Context;

import Model.Account;
import Model.CVMentor;
import Model.Review;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;

public class ReviewDAO extends DBContext{
    public int countRating(int rating, int mentor_id) {
        int count = 0;
        try {
            String sql = "SELECT COUNT(*) AS count FROM reviews WHERE rating = ? AND  mentor_id=?";
           PreparedStatement ps = connection.prepareStatement(sql);
            ps.setInt(1, rating);
            ps.setInt(2, mentor_id);
            ResultSet resultSet = ps.executeQuery();
            if (resultSet.next()) {
                count = resultSet.getInt(1);
            }
        } catch (SQLException e) {
            // Handle the exception or log an error message
        }
        return count;
    }    
   public  ArrayList<Review> getReview(int mentor_id) {
        ArrayList<Review> reviewList = new ArrayList<>();
        try {
            String sql = "select r.*, a.user_name from reviews r, users a where r.mentor_id = ? and a.user_id = r.user_id;";
            PreparedStatement ps = connection.prepareStatement(sql);
            ps.setInt(1, mentor_id);
            ResultSet resultSet = ps.executeQuery();
             while (resultSet.next()) {
            int review_id = resultSet.getInt("review_id");
            int user_id = resultSet.getInt("user_id");
            int rating = resultSet.getInt("rating");
            String comment_review = resultSet.getString("comment_review");
            Date date_comment = resultSet.getDate("date_comment");
            String user_name = resultSet.getString("user_name");

            Account account = new Account(user_id, user_name, "", "", 3);
            CVMentor mentor = new CVMentor(mentor_id, "", null, false, "", "", "", "", "", "", "");
            Review review = new Review(review_id, account, mentor, rating, comment_review, date_comment);
            reviewList.add(review);
        }
        } catch (SQLException e) {
            Logger.getLogger(DBContext.class.getName()).log(Level.SEVERE, null, e);
        }
        return reviewList;
    }
  public boolean insertRating(Review r) {
    try {
        String sql = "INSERT INTO reviews (user_id, rating, comment_review, mentor_id, date_comment) VALUES (?, ?, ?, ?, ?)";
        PreparedStatement stm = connection.prepareStatement(sql);
        stm.setInt(1, r.getAccount().getId());
        stm.setInt(2, r.getRating());
        stm.setNString(3, r.getComment());
        stm.setInt(4, r.getMentor().getMentorId());
        stm.setDate(5, (Date) r.getCommentDate());
        stm.executeUpdate();
        return true;
    } catch (SQLException e) {
        Logger.getLogger(DBContext.class.getName()).log(Level.SEVERE, null, e);
    }
    return false;
}


   public boolean allowReview(int userID, int mentorID) {
    try {
        String sql = "SELECT COUNT(*) FROM reviews WHERE user_id = ? AND mentor_id = ?";
        PreparedStatement stm = connection.prepareStatement(sql);
        stm.setInt(1, userID);
        stm.setInt(2, mentorID);
        ResultSet rs = stm.executeQuery();
        if (rs.next()) {
            int reviewCount = rs.getInt(1);
            return (reviewCount == 0);
        }
    } catch (SQLException e) {
        Logger.getLogger(DBContext.class.getName()).log(Level.SEVERE, null, e);
    }
    return false;
}

}
