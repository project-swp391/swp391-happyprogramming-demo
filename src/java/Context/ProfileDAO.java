/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package Context;

import Model.Profile;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Admin
 */
public class ProfileDAO extends DBContext{
    public Profile getProfile(int user_id) {
        String xSql = "select * from Profile where user_id = ? ";
        try {
            PreparedStatement ps = connection.prepareStatement(xSql);
            ps.setInt(1, user_id);
            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                return new Profile(rs.getString(3), rs.getString(4), rs.getBoolean(5), rs.getString(6), rs.getDate(7), rs.getString(8), rs.getString(9));
            }
        } catch (SQLException ex) {
            Logger.getLogger(SkillDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }
    public void updateProfileMentee(String fullname, boolean sex, String phone, Date dob, String address, String description, int user_id) {
        
        try {
            String xSql = "UPDATE Profile\n"
                + " SET   full_name = ?\n"
                + "      ,sex = ?\n"
                + "      ,phone = ? \n"
                + "      ,dob = ?\n"
                + "      ,address = ?  \n"
                + "      ,description = ? \n"
                + " WHERE user_id = ? ";
            PreparedStatement ps = connection.prepareStatement(xSql);
            ps.setString(1, fullname);
            ps.setBoolean(2, sex);
            ps.setString(3, phone);
            ps.setDate(4, dob);
            ps.setString(5, address);
            ps.setString(6, description);
            ps.setInt(7, user_id);
            ps.executeUpdate();
        } catch (SQLException ex) {
            Logger.getLogger(SkillDAO.class.getName()).log(Level.SEVERE, null, ex);
        }

    }
    public void updateImage(int user_id, String imagePath) {
        
        try {
            String xSql = "UPDATE Profile SET avatar = ? WHERE user_id = ? ";
            PreparedStatement ps = connection.prepareStatement(xSql);
            ps.setString(1, imagePath);
            ps.setInt(2, user_id);
            ps.executeUpdate();
        } catch (SQLException ex) {
            Logger.getLogger(SkillDAO.class.getName()).log(Level.SEVERE, null, ex);
        }

    }
    
}
