/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/JSP_Servlet/Servlet.java to edit this template
 */
package Controller.Mentor;

import Context.MyMentorDAO;
import Model.Account;
import Model.DAO;
import Model.Mentor;
import java.io.IOException;
import java.io.PrintWriter;
import jakarta.servlet.ServletException;
import jakarta.servlet.annotation.MultipartConfig;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import jakarta.servlet.http.HttpSession;
import jakarta.servlet.http.Part;
import java.io.File;

@MultipartConfig
@WebServlet(name = "EditImageMentor", urlPatterns = {"/imagementor"})
public class imagementor extends HttpServlet {

    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try ( PrintWriter out = response.getWriter()) {
            out.println("<!DOCTYPE html>");
            out.println("<html>");
            out.println("<head>");
            out.println("<title>Servlet imagementor</title>");
            out.println("</head>");
            out.println("<body>");
            out.println("<h1>Servlet imagementor at " + request.getContextPath() + "</h1>");
            out.println("</body>");
            out.println("</html>");
        }
    }

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        try {
            // lay duong link dan cua anh
            Part part = request.getPart("fileimage");
            if (part == null || part.getSize() == 0) {
                response.sendRedirect("editprofile");
            } else {
                String realPath = request.getServletContext().getRealPath("/images");
                String submittedFileName = part.getSubmittedFileName();
                String filename = submittedFileName.substring(submittedFileName.lastIndexOf('/') + 1)
                        .substring(submittedFileName.lastIndexOf('\\') + 1);
                File directory = new File(realPath);
                if (!directory.exists()) {
                    directory.mkdirs();
                }
                part.write(realPath + File.separator + filename);
                String imagePath = "images/" + filename;
                MyMentorDAO mDAO = new MyMentorDAO();
                HttpSession session = request.getSession();
                Account a = (Account) session.getAttribute("acc");
                Mentor m = mDAO.getMentorbyUserID(a.getId());
                mDAO.updateImageMentor(m.getId(), imagePath);
                response.sendRedirect("updatementor");
            }
        } catch (ServletException | IOException e) {
            System.out.println(e);
        }

    }
}
