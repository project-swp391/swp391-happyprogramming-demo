/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/JSP_Servlet/Servlet.java to edit this template
 */
package Controller;

import Context.AccountDAO;
import Model.Account;
import java.io.IOException;
import jakarta.servlet.ServletException;
import jakarta.servlet.http.Cookie;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import jakarta.servlet.http.HttpSession;
import java.time.LocalDateTime;

/**
 *
 * @author Admin
 */
public class LoginControl extends HttpServlet {

    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        String username = request.getParameter("user");
        String password = request.getParameter("pass");
        String remember = request.getParameter("remember");
        AccountDAO dao = new AccountDAO();
        Account a = dao.login(username, password);
        if (a == null) {
            request.setAttribute("mess", "Wrong User or Password !!!");
            request.getRequestDispatcher("login.jsp").forward(request, response);
        } else {
            HttpSession session = request.getSession();
            session.setAttribute("acc", a);
            if (a.getRole_id() != 1) {
                session.setMaxInactiveInterval(30000);
                Cookie u = new Cookie("user", username);
                Cookie p = new Cookie("pass", password);
                u.setMaxAge(60 * 60 * 24);
                if (remember != null) {
                    p.setMaxAge(60 * 60 * 24);
                } else {
                    p.setMaxAge(0);
                }
                response.addCookie(u);
                response.addCookie(p);
                LocalDateTime expirationTime = (LocalDateTime) request.getSession().getAttribute("passwordExpiration");
                if (expirationTime != null && LocalDateTime.now().isAfter(expirationTime)) {
                    request.setAttribute("passwordExpired", true);
                    request.getRequestDispatcher("login.jsp").forward(request, response);

                } else {
                    response.sendRedirect("home1.jsp");
                }
            } else {
                response.sendRedirect("home1.jsp");
            }

        }
    }

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        Cookie arr[] = request.getCookies();
        if (arr != null) {
            for (Cookie o : arr) {
                if (o.getName().equals("user")) {
                    request.setAttribute("username", o.getValue());
                }
                if (o.getName().equals("pass")) {
                    request.setAttribute("password", o.getValue());
                }
            }
        }
        request.getRequestDispatcher("login.jsp").forward(request, response);
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

}
