/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/JSP_Servlet/Servlet.java to edit this template
 */
package Controller;

import Context.AccountDAO;
import Model.Account;
import Model.DAO;
import java.io.IOException;
import java.io.PrintWriter;
import jakarta.servlet.ServletException;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;

/**
 *
 * @author Admin
 */
@WebServlet(name = "changeppasscontrol", urlPatterns = {"/changepass"})
public class changeppasscontrol extends HttpServlet {

    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try ( PrintWriter out = response.getWriter()) {
            /* TODO output your page here. You may use following sample code. */
            out.println("<!DOCTYPE html>");
            out.println("<html>");
            out.println("<head>");
            out.println("<title>Servlet changeppasscontrol</title>");
            out.println("</head>");
            out.println("<body>");
            out.println("<h1>Servlet changeppasscontrol at " + request.getContextPath() + "</h1>");
            out.println("</body>");
            out.println("</html>");
        }
    }

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        String user = request.getParameter("user");
        String oldpass = request.getParameter("oldpass");
        String newpass = request.getParameter("newpass");
        if (newpass.length() < 6) {
            request.setAttribute("mess", "Mật khẩu phải có ít nhất 6 ký tự.");
            request.getRequestDispatcher("changepass.jsp").forward(request, response);
        } else {
            AccountDAO d = new AccountDAO();
            Account a = d.login(user, oldpass);
            if (a == null) {
                request.setAttribute("mess", "Vui lòng kiểm tra tài khoản hoặc mật khẩu !!!");
                request.getRequestDispatcher("changepass.jsp").forward(request, response);
            } else {
                d.updatePass(newpass, user);
                response.sendRedirect("login");
            }
        }

    }

    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
