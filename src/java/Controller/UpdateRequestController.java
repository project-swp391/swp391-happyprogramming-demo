/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/JSP_Servlet/Servlet.java to edit this template
 */
package Controller;

import Context.MyMentorDAO;
import Context.RequestDAO;
import Context.RequestMentorDAO;
import Context.Request_SkillDAO;
import Model.Account;
import Model.MyMentor;
import Model.Request1;
import Model.RequestMentor;
import java.io.IOException;
import java.io.PrintWriter;
import jakarta.servlet.ServletException;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import java.sql.Date;
import java.time.LocalDate;
import java.util.ArrayList;

/**
 *
 * @author dell
 */
public class UpdateRequestController extends HttpServlet {

    String action;

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try ( PrintWriter out = response.getWriter()) {
            out.println("<!DOCTYPE html>");
            out.println("<html>");
            out.println("<head>");
            out.println("<title>Servlet UpdateRequestController</title>");
            out.println("</head>");
            out.println("<body>");
            out.println("<h1>Servlet UpdateRequestController at " + request.getContextPath() + "</h1>");
            out.println("</body>");
            out.println("</html>");
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        int rqId = Integer.parseInt(request.getParameter("rid"));

        RequestDAO rDao = new RequestDAO();
        Request1 rq = rDao.getRequestByID(rqId);

        RequestMentorDAO rmDao = new RequestMentorDAO();
        ArrayList<RequestMentor> listRM = rmDao.getAll();

        MyMentorDAO mDao = new MyMentorDAO();
        ArrayList<MyMentor> listM = mDao.getMenterbyStatus();

        Date date = Date.valueOf(LocalDate.now());

        request.setAttribute("now", date);
        request.setAttribute("listRM", listRM);
        request.setAttribute("listM", listM);
        request.setAttribute("rq", rq);
        request.getRequestDispatcher("DetailRequest.jsp").forward(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        Request1 rq = new Request1();
        RequestMentor rm = new RequestMentor();
        RequestDAO rDao = new RequestDAO();
        Request_SkillDAO rsDao = new Request_SkillDAO();

        RequestMentorDAO rmDao = new RequestMentorDAO();
        int reqID;

        action = request.getParameter("action");
        switch (action) {
            case "update":
                reqID = Integer.parseInt(request.getParameter("reqID"));

                int mentorID = Integer.parseInt(request.getParameter("mentorID"));
                String title = request.getParameter("title");
                String content = request.getParameter("content");
                Date deadlineDate = Date.valueOf(request.getParameter("deadlineDate"));
                String time = request.getParameter("deadlineHour");
                double hour = Double.parseDouble(time);

                Account mentor = new Account();
                mentor.setId(mentorID);

                rq.setId(reqID);
                rq.setContent(content);
                rq.setTitle(title);
                rq.setDeadlineDate(deadlineDate);
                rq.setDeadlineHour(hour);

                rDao.update(rq);

                rm.setMentor(mentor);
                rm.setRequest(rq);
                rmDao.update(rm);

                response.sendRedirect("request");
                break;
            case "delete":
                reqID = Integer.parseInt(request.getParameter("reqID"));
                rq.setId(reqID);
                rm.setRequest(rq);

                rq.setId(reqID);
                rm.setRequest(rq);

                rsDao.delete(rq);
                rmDao.delete(rm);
                rDao.delete(rq);
                response.sendRedirect("request");
                break;
        }
    }

//    public static void main(String[] args) {
//        Request rq = new Request();
//        rq.setId(1);
//
//        RequestDAO rDao = new RequestDAO();
//        rDao.delete(rq);
//    }
    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
