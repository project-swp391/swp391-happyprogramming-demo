/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/JSP_Servlet/Servlet.java to edit this template
 */
package Controller.Mentee;

import Context.ProfileDAO;
import Model.Account;
import Model.DAO;
import java.io.IOException;
import jakarta.servlet.ServletException;
import jakarta.servlet.annotation.MultipartConfig;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import jakarta.servlet.http.HttpSession;
import jakarta.servlet.http.Part;
import java.io.File;

/**
 *
 * @author Admin
 */
@MultipartConfig()
@WebServlet(name = "UploadImage", urlPatterns = {"/upimage"})
public class UploadImageController extends HttpServlet {

    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");

    }

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        request.getRequestDispatcher("updateprofilementee.jsp").forward(request, response);

    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        try {
            Part part = request.getPart("fileimage");
            if (part == null || part.getSize() == 0) {
                response.sendRedirect("editprofile");
            } else {
                String realPath = request.getServletContext().getRealPath("/images");
                String submittedFileName = part.getSubmittedFileName();
                String filename = submittedFileName.substring(submittedFileName.lastIndexOf('/') + 1)
                        .substring(submittedFileName.lastIndexOf('\\') + 1);
                File directory = new File(realPath);
                if (!directory.exists()) {
                    directory.mkdirs();
                }
                part.write(realPath + File.separator + filename);
                String imagePath = "images/" + filename;
                ProfileDAO pDAO = new ProfileDAO();
                HttpSession session = request.getSession();
                Account a = (Account) session.getAttribute("acc");
                pDAO.updateImage(a.getId(), imagePath);
                response.sendRedirect("editprofile");
            }
        } catch (ServletException | IOException e) {
            System.out.println(e);
        }

    }

    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
