/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package Model;

/**
 *
 * @author dell
 */
public class MentorSkill {
    private int mentorId;
    private  int skillId;

    public MentorSkill() {
    }

    public MentorSkill(int mentorId, int skillId) {
        this.mentorId = mentorId;
        this.skillId = skillId;
    }

    public int getMentorId() {
        return mentorId;
    }

    public void setMentorId(int mentorId) {
        this.mentorId = mentorId;
    }

    public int getSkillId() {
        return skillId;
    }

    public void setSkillId(int skillId) {
        this.skillId = skillId;
    }

    @Override
    public String toString() {
        return "MentorSkill{" + "mentorId=" + mentorId + ", skillId=" + skillId + '}';
    }
    
    
    

    
    
    
}
