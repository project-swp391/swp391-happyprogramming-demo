/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package Model;

/**
 *
 * @author Admin
 */
public class MyMentor {
    private int id ;
    private Account mentor;
    private StatusMentor status;
    private Skill skill;

    public MyMentor() {
    }

    public MyMentor(int id, Account mentor, StatusMentor status, Skill skill) {
        this.id = id;
        this.mentor = mentor;
        this.status = status;
        this.skill = skill;
    }

    public MyMentor(int id, Account mentor, StatusMentor status) {
        this.id = id;
        this.mentor = mentor;
        this.status = status;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public Account getMentor() {
        return mentor;
    }

    public void setMentor(Account mentor) {
        this.mentor = mentor;
    }

    public StatusMentor getStatus() {
        return status;
    }

    public void setStatus(StatusMentor status) {
        this.status = status;
    }

    public Skill getSkill() {
        return skill;
    }

    public void setSkill(Skill skill) {
        this.skill = skill;
    }

    @Override
    public String toString() {
        return "MyMentor{" + "id=" + id + ", mentor=" + mentor + ", status=" + status + ", skill=" + skill + '}';
    }
    
    
}
