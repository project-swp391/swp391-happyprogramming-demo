/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package Model;

/**
 *
 * @author PC
 */
import java.util.Date;

public class Review {
    private int reviewId;
    private Account account;
    private CVMentor mentor;
    private int rating;
    private String comment;
    private Date commentDate;

    public Review() {
    }

    public Review(int reviewId, Account account, CVMentor mentor, int rating, String comment, Date commentDate) {
        this.reviewId = reviewId;
        this.account = account;
        this.rating = rating;
        this.comment = comment;
        this.commentDate = commentDate;
        this.mentor = mentor;
    }

    // Getters and setters
    public int getReviewId() {
        return reviewId;
    }

    public void setReviewId(int reviewId) {
        this.reviewId = reviewId;
    }

    public Account getAccount() {
        return account;
    }

    public void setAccount(Account account) {
        this.account = account;
    }

    public CVMentor getMentor() {
        return mentor;
    }

    public void setMentor(CVMentor mentor) {
        this.mentor = mentor;
    }

    public int getRating() {
        return rating;
    }

    public void setRating(int rating) {
        this.rating = rating;
    }

    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }

    public Date getCommentDate() {
        return commentDate;
    }

    public void setCommentDate(Date commentDate) {
        this.commentDate = commentDate;
    }
}

