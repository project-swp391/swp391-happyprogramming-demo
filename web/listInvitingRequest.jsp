<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <title>List of Invited Requests</title>

        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css">
        <style>


            .request-card {
                border-radius: 10px;
                box-shadow: 0 2px 6px rgba(0, 0, 0, 0.1);
                margin-bottom: 20px;
            }
            .request-card .card-header {
                background-color: #f8f9fa;
                font-weight: bold;
            }
            .request-card .card-body {
                padding: 20px;
            }
            .request-card .btn-action {
                margin-right: 10px;
                font-weight: bold;
            }
        </style>
    </head>
    <body>
        <jsp:include page="menu.jsp"></jsp:include>
            <div class="container">
                <h1 class="mt-5 mb-4"></h1>

                <div class="row">
                
                <c:forEach var="r"  items="${listR}">
                    <div class="col-md-12">
                        <div class="card request-card">
                            <div class="card-header">
                                <h5 class="card-title">${r.title}</h5>
                            </div>
                            <div class="card-body">
                                <p class="card-text">Deadline Date: ${r.deadlineDate}</p>
                                <p class="card-text">Deadline Hour: ${r.deadlineHour}</p>
                                <p class="card-text">Content: ${r.content}.</p>
                                <p class="card-text">Skill: 
                                    
                                    <c:forEach var="rs" items="${listRS}" varStatus="dem">
                                       
                                        <c:if test="${r.getId() == rs.getRequest().getId()}">
                                            <c:forEach var="s" items="${listS}">
                                                <c:if test="${rs.getSkill().getId() == s.id}">     
                                                    ${s.name},
                                                </c:if>
                                            </c:forEach> 
                                        </c:if>
                                    </c:forEach>
                                 
                                                    
                                </p>

                                <button class="btn btn-success btn-action"  onclick="window.location.href = 'acceptRequest?id=${r.getId()}'">Accept</button>
                                <button class="btn btn-danger btn-action" onclick="window.location.href = 'rejectRequest?id=${r.getId()}'">Reject</button>
                            </div>
                        </div>
                    </div>
                </c:forEach>

            </div>
        </div>

        <!-- Add Bootstrap JS (optional) -->
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.min.js"></script>
        <script>
                                    
                                    if (${listR} === null) {
                                       
                                        document.body.style.backgroundImage = 'url(https://png.pngtree.com/png-clipart/20190924/original/pngtree-empty-box-icon-for-your-project-png-image_4849557.jpg)';
                                    } 
        </script>
    </body>
</html>