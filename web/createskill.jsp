
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <title>Fixed Menu and Scrollable Content</title>
        <link rel="stylesheet" type="text/css" href="styles.css">
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.15.3/css/all.min.css">
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/css/bootstrap.min.css">
    </head>
    <style>
        body {
            overflow: hidden;
            padding: 15px;
            font-family: Arial, sans-serif;
            background-color: #f8f8f8;
            background-image: url('https://phunugioi.com/wp-content/uploads/2020/08/hinh-nen-mau-trang.jpg');
        }

        .container {
            max-width: 500px;
            margin: 0 auto;
            padding: 20px;
            background-color: #fff;
            border-radius: 5px;
            box-shadow: 0 2px 5px rgba(0, 0, 0, 0.1);
        }

        h1 {
            text-align: center;
            margin-bottom: 20px;
        }

        label {
            display: block;
            font-weight: bold;
            margin-bottom: 5px;
        }

        input[type="text"] {
            width: 100%;
            padding: 10px;
            border: 1px solid #ccc;
            border-radius: 4px;
            box-sizing: border-box;
        }

        select {
            width: 100%;
            padding: 10px;
            border: 1px solid #ccc;
            border-radius: 4px;
            box-sizing: border-box;
        }

        .btn-custom {
            display: block;
            width: 100%;
            padding: 10px;
            background-color: #4CAF50;
            color: #fff;
            border: none;
            border-radius: 4px;
            cursor: pointer;
            transition: background-color 0.3s ease;
        }

        .btn-custom:hover {
            background-color: #45a049;
        }
        .slideshow-container {
            max-width: 100%;
            position: relative;
            margin: auto;
        }

        .slide {
            display: none;
            position: absolute;
            top: 0;
            left: 0;
            width: 100%;
        }

        .slide img {
            width: 100%;
            height: auto;
        }
        .menu {
            height: 100vh;
            background-color: #545b62;
            padding: 20px;
        }
        .menu .list-group-item {
            transition: background-color 0.3s;
        }
        .avatar {
            display: flex;
            flex-direction: column;
            align-items: center;
            margin-bottom: 20px;
        }

        .avatar-img {
            width: 150px;
            height: 150px;
            border-radius: 50%;
        }

        .username {
            margin-top: 10px;
            font-weight: bold;
            color:white;
        }


        .menu .list-group-item:hover {
            background-color: #e9ecef;
        }

        .menu .list-group-item.active {
            background-color: #007bff;
            color: #fff;
        }


        .content {
            height: 100vh;
            overflow-y: scroll;
            padding: 20px;
        }

        .scrollable-content {
            height: 100%;
        }

    </style>
    <body>
        <div class="container-fluid">
            <div class="row">
                <div class="col-md-3 menu">
                    <div class="avatar">
                        <img src="https://antimatter.vn/wp-content/uploads/2022/10/hinh-anh-gai-xinh-de-thuong.jpg" alt="Avatar" class="avatar-img">
                        <div class="username">Admin</div>
                    </div>
                    <ul class="list-group">
                        <li class="list-group-item">
                            <a class="nav-link" href="#">
                                <i class="fas fa-home"></i> Trang chủ
                            </a>
                        </li>
                        <li class="list-group-item active" >
                            <a class="nav-link text-white" href="#" >
                                <i class="fas fa-list-alt"></i> Create Skill
                            </a>
                        </li>
                        <li class="list-group-item">
                            <a class="nav-link" href="list-all-mentor">
                                <i class="fas fa-users"></i> Manager all mentor
                            </a>
                        </li>
                        <li class="list-group-item">
                            <a class="nav-link" href="#">
                                <i class="fas fa-users"></i> Thành viên
                            </a>
                        </li>
                        <li class="list-group-item">
                            <a class="nav-link" href="#">
                                <i class="fas fa-cogs"></i> Cài đặt
                            </a>
                        </li>
                    </ul>
                </div>
                <div class="col-md-9 content">
                    <div class="scrollable-content">
                        <h1 class="mb-4">Create Skill</h1>
                        <form action="createskill" method="post">
                            <div class="mb-2">
                                <label for="skillName" class="form-label">Skill Name</label>
                                <input type="text" class="form-control" id="skillName" name="name" required>
                            </div>
                            <div class="mb-2">
                                <label for="price" class="form-label">Price</label>
                                <input type="text" class="form-control" name="price" required>
                            </div>

                            <div class="mb-2">
                                <label for="url" class="form-label">URL Image</label>
                                <input type="text" class="form-control"  name="url" required>
                            </div>
                            <div class="mb-2">
                                <label for="skillName" class="form-label">description</label>
                                <textarea name="description"
                                          class="form-control ng-pristine ng-valid ng-empty ng-touched" cols="20"
                                          rows="2"  ></textarea>
                            </div>

                            <div class="mb-2">
                                <label for="status" class="form-label">Status</label>
                                <select class="form-select"  name="status">
                                    <option value="active" selected>Active</option>
                                    <option value="inactive">Inactive</option>
                                </select>
                            </div>
                            <button type="submit" class="btn btn-custom">OK</button>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </body>
</html>






