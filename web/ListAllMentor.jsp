<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <title>Fixed Menu and Scrollable Content</title>
        <link rel="stylesheet" type="text/css" href="styles.css">
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.15.3/css/all.min.css">
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/css/bootstrap.min.css">
    </head>
    <style>

        .menu {
            height: 100vh;
            background-color: #545b62;
            padding: 20px;
        }
        .menu .list-group-item {
            transition: background-color 0.3s;
        }
        .avatar {
            display: flex;
            flex-direction: column;
            align-items: center;
            margin-bottom: 20px;
        }

        .avatar-img {
            width: 150px;
            height: 150px;
            border-radius: 50%;
        }

        .username {
            margin-top: 10px;
            font-weight: bold;
            color:white;
        }


        .menu .list-group-item:hover {
            background-color: #e9ecef;
        }

        .menu .list-group-item.active {
            background-color: #007bff;
            color: #fff;
        }


        .content {
            height: 100vh;
            overflow-y: scroll;
            padding: 6px;
        }

        .scrollable-content {
            height: 100%;
        }
        body {
            overflow: hidden;
            font-family: Arial, sans-serif;
            margin: 0;
            padding: 15px;
            margin-bottom: 150px;
            background-image: url('https://phunugioi.com/wp-content/uploads/2020/08/hinh-nen-mau-trang.jpg');
            background-size: cover;
            background-position: center;
        }

        h1 {
            text-align: center;
            color: #333;
        }

        form {
            margin-top: 20px;
            text-align: center;
        }

        input[type="text"] {
            padding: 8px;
            width: 250px;
            border: 1px solid #ddd;
            border-radius: 4px;
            font-size: 16px;
        }

        button[type="submit"] {
            padding: 8px 16px;
            background-color: #4CAF50;
            color: white;
            border: none;
            border-radius: 4px;
            cursor: pointer;
            font-size: 16px;
        }

        table {
            width: 100%;
            border-collapse: collapse;
            margin-top: 20px;
            background-color: #fff;
        }

        th, td {
            padding: 12px 8px;
            text-align: left;
            border-bottom: 1px solid #ddd;
        }

        th {
            background-color: #f2f2f2;
            font-weight: bold;
            color: #333;
        }

        .pagination {
            display: flex;
            justify-content: center;
            margin-top: 20px;
        }

        .pagination a {
            color: #333;
            background-color: #ddd;
            padding: 8px 16px;
            text-decoration: none;
            border: 1px solid #ddd;
            margin: 0 4px;
        }

        .pagination a.active {
            background-color: #4CAF50;
            color: white;
            border: 1px solid #4CAF50;
        }

        .pagination a:hover:not(.active) {
            background-color: #ddd;
            color: white;
        }

    </style>
    <body>
        <div class="container-fluid">
            <div class="row">
                <div class="col-md-3 menu">
                    <div class="avatar">
                        <img src="https://antimatter.vn/wp-content/uploads/2022/10/hinh-anh-gai-xinh-de-thuong.jpg" alt="Avatar" class="avatar-img">
                        <div class="username">Admin</div>
                    </div>
                    <ul class="list-group">
                        <li class="list-group-item">
                            <a class="nav-link" href="#">
                                <i class="fas fa-home"></i> Trang chủ
                            </a>
                        </li>
                        <li class="list-group-item " >
                            <a class="nav-link" href="createskill.jsp" >
                                <i class="fas fa-list-alt"></i> Create Skill
                            </a>
                        </li>
                        <li class="list-group-item active">
                            <a class="nav-link text-white" href="list-all-mentor">
                                <i class="fas fa-users "></i> Manager all mentor
                            </a>
                        </li>
                        <li class="list-group-item">
                            <a class="nav-link" href="#">
                                <i class="fas fa-users"></i> Thành viên
                            </a>
                        </li>
                        <li class="list-group-item">
                            <a class="nav-link" href="#">
                                <i class="fas fa-cogs"></i> Cài đặt
                            </a>
                        </li>
                    </ul>
                </div>
                <div class="col-md-9 content">
                    <div class="scrollable-content">
                        <form>
                            <input type="text" placeholder="Search..." name="search">
                            <button type="submit">Search</button>
                        </form>
                        <div class="table table-striped" >
                            <table>
                                <thead class="thead-dark">
                                    <tr>
                                        <th>STT</th>
                                        <th>ID</th>
                                        <th>Fullname</th>
                                        <th>Account Name</th>
                                        <th>Profession</th>
                                        <th>Accepted Requests</th>
                                        <th>Completed</th>
                                        <th>Rate Star</th>
                                        <th>Action</th>
                                    </tr>
                                </thead>
                                <tbody>
                                <c:forEach var="cv" items="${listCV}" varStatus="loop">
                                    <tr>
                                        <td>${loop.count}</td>
                                        <td>${cv.mentorId}</td>
                                        <td>${cv.fullname}</td>
                                    <c:forEach var="m" items="${listM}">
                                        <c:if test="${cv.mentorId == m.id}">
                                            <td>${m.getMentor().getUser_name()}</td>
                                        </c:if>
                                    </c:forEach>
                                    <td>${cv.job}</td>

                                    <c:forEach var="cr" items="${listCR}">
                                        <c:if test="${cv.mentorId == cr.getMentorId()}" >
                                            <td>${cr.getCount()}</td>
                                        </c:if>
                                    </c:forEach>

                                    <c:forEach var="pr" items="${listPR}">
                                        <c:if test="${cv.mentorId == pr.getMentorId()}">
                                            <td>${pr.getPercent()}%</td>
                                        </c:if>

                                    </c:forEach>

                                    <td>4.5</td>
                                    <c:forEach var="m" items="${listM}">
                                        <c:if test="${cv.mentorId == m.id}">
                                            <c:if test="${m.getStatus().getId() == 1}">
                                                <td >
                                                    <button type="button" onclick="window.location.href = 'update-status-mentor?id=${cv.mentorId}&sid=${m.getStatus().getId()}'" style="background-color: green; color: white;">
                                                        <i class="fas fa-check-circle" ></i> Active</button>
                                                </td>
                                            </c:if>
                                            <c:if test="${m.getStatus().getId() == 2}">
                                                <td>
                                                    <button type="button" onclick=" window.location.href = 'update-status-mentor?id=${cv.mentorId}&sid=${m.getStatus().getId()}'" style="background-color: gray; color: white;">
                                                        <i class='fa-solid fa-circle-xmark'></i> Inactive</button>
                                                </td>
                                            </c:if>
                                        </c:if>
                                    </c:forEach>
                                    </tr>

                                </c:forEach>
                                <tr>
                                    <td>3</td>
                                    <td>867</td>
                                    <td>John Doe</td>
                                    <td>johndoe</td>
                                    <td>Engineer</td>
                                    <td>5</td>
                                    <td>80%</td>
                                    <td>4.5</td>
                                    <td>
                                        <button type="button" style="background-color: green; color: white;">
                                            <i class="fas fa-check-circle"></i> Active</button>
                                    </td>
                                </tr>
                                <tr>
                                    <td>4</td>
                                    <td>219</td>
                                    <td>John Doe</td>
                                    <td>johndoe</td>
                                    <td>Engineer</td>
                                    <td>5</td>
                                    <td>80%</td>
                                    <td>4.5<i class="bi bi-star"></i></td>
                                    <td>
                                        <button type="button" style="background-color: green; color: white;"><i class="fas fa-check-circle"></i> Active</button>
                                    </td>
                                </tr>
                                <tr>
                                    <td>5</td>
                                    <td>913</td>
                                    <td>John Doe</td>
                                    <td>johndoe</td>
                                    <td>Engineer</td>
                                    <td>5</td>
                                    <td>80%</td>
                                    <td>4.5<i class="bi bi-star"></i></td>
                                    <td>
                                        <button type="button" style="background-color: green; color: white;"><i class="fas fa-check-circle"></i> Active</button>
                                    </td>
                                </tr>
                                <tr>
                                    <td>6</td>
                                    <td>15213</td>
                                    <td>John Doe</td>
                                    <td>johndoe</td>
                                    <td>Engineer</td>
                                    <td>5</td>
                                    <td>80%</td>
                                    <td>4.5<i class="bi bi-star"></i></td>
                                    <td>
                                        <button type="button" style="background-color: green; color: white;"><i class="fas fa-check-circle"></i> Active</button>
                                    </td>
                                </tr>
                                <tr>
                                    <td>7</td>
                                    <td>12341</td>
                                    <td>John Doe</td>
                                    <td>johndoe</td>
                                    <td>Engineer</td>
                                    <td>5</td>
                                    <td>80%</td>
                                    <td>4.5<i class="bi bi-star"></i></td>
                                    <td>
                                        <button type="button" style="background-color: green; color: white;"><i class="fas fa-check-circle"></i> Active</button>
                                    </td>
                                </tr>
                                </tbody>
                            </table>
                        </div>

                        <div class="pagination">
                            <a href="#"><i class="fas fa-angle-double-left"></i></a>
                            <a href="#" class="active">1</a>
                            <a href="#" >2</a>
                            <a href="#">3</a>
                            <a href="#"><i class="fas fa-angle-double-right"></i></a>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </body>
</html>






