
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <title>Fixed Menu and Scrollable Content</title>
        <link rel="stylesheet" type="text/css" href="styles.css">
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.15.3/css/all.min.css">
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/css/bootstrap.min.css">
    </head>
    <style>

        .menu {
            height: 100vh;
            background-color: #545b62;
            padding: 20px;
        }
        .menu .list-group-item {
            transition: background-color 0.3s;
        }
        .avatar {
            display: flex;
            flex-direction: column;
            align-items: center;
            margin-bottom: 20px;
        }

        .avatar-img {
            width: 150px;
            height: 150px;
            border-radius: 50%;
        }

        .username {
            margin-top: 10px;
            font-weight: bold;
            color:white;
        }


        .menu .list-group-item:hover {
            background-color: #e9ecef;
        }

        .menu .list-group-item.active {
            background-color: gray;
            color: #fff;
        }


        .content {
            height: 100vh;
            overflow-y: scroll;
            padding: 20px;
        }

        .scrollable-content {
            height: 100%;
        }
        

    </style>
    <body>
        <div class="container-fluid">
            <div class="row">
                <div class="col-md-3 menu">
                    <div class="avatar">
                        <img src="https://antimatter.vn/wp-content/uploads/2022/10/hinh-anh-gai-xinh-de-thuong.jpg" alt="Avatar" class="avatar-img">
                        <div class="username">Admin</div>
                    </div>
                    <ul class="list-group">
                        <li class="list-group-item">
                            <a class="nav-link" href="#">
                                <i class="fas fa-home"></i> Trang chủ
                            </a>
                        </li>
                        <li class="list-group-item " >
                            <a class="nav-link" href="createskill.jsp" >
                                <i class="fas fa-list-alt"></i> Create Skill
                            </a>
                        </li>
                        <li class="list-group-item active">
                            <a class="nav-link text-white" href="list-all-mentor">
                                <i class="fas fa-users "></i> Manager mentor
                            </a>
                        </li>
                        <li class="list-group-item">
                            <a class="nav-link" href="">
                                <i class="fas fa-book"></i> Manager skills
                            </a>
                        </li>
                        <li class="list-group-item">
                            <a class="nav-link" href="#">
                                <i class="fas fa-cogs"></i> Cài đặt
                            </a>
                        </li>
                    </ul>
                </div>
                <div class="col-md-9 content">
                    <div class="scrollable-content">
                        

                    </div>
                </div>
            </div>
        </div>
    </body>
</html>






