

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib uri = "http://java.sun.com/jsp/jstl/core" prefix = "c" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<!DOCTYPE html>
<html lang="zxx" class="no-js">

    <head>
        <!-- Mobile Specific Meta -->
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no" />
        <!-- Favicon -->
        <link rel="shortcut icon" href="img/fav.png" />
        <!-- Author Meta -->
        <meta name="author" content="colorlib" />
        <!-- Meta Description -->
        <meta name="description" content="" />
        <!-- Meta Keyword -->
        <meta name="keywords" content="" />
        <!-- meta character set -->
        <meta charset="UTF-8" />
        <!-- Site Title -->
        <title>Happy Programming</title>

        <link href="https://fonts.googleapis.com/css?family=Playfair+Display:900|Roboto:400,400i,500,700" rel="stylesheet" />
        <!--
            CSS
            =============================================
        -->
        <link rel="stylesheet" href="css/linearicons.css" />
        <link rel="stylesheet" href="css/font-awesome.min.css" />
        <link rel="stylesheet" href="css/bootstrap.css" />
        <link rel="stylesheet" href="css/magnific-popup.css" />
        <link rel="stylesheet" href="css/owl.carousel.css" />
        <link rel="stylesheet" href="css/nice-select.css">
        <link rel="stylesheet" href="css/hexagons.min.css" />
        <link rel="stylesheet" href="https://cdn.jsdelivr.net/themify-icons/0.1.2/css/themify-icons.css" />
        <link rel="stylesheet" href="css/main.css" />
        <link rel="stylesheet" href="https://unpkg.com/@webpixels/css/dist/index.css">
        <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.1/dist/css/bootstrap.min.css">
        <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.1/dist/js/bootstrap.bundle.min.js">
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.min.js">
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css">
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.bundle.min.js">
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.min.js">
        <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.7.2/css/all.css">
        <style>
          
            *{
                padding: 0;
                margin: 0;
                box-sizing: border-box;
                font-family: 'Poppins', sans-serif;
                font-size: 0.95rem;
            }
            body{
                background-color: #f3f3f3;
            }
            #starred{
                box-shadow: 3px 3px 10px #b5b5b5;
            }
            .table div.text-muted{
                font-size: 0.85rem;
                font-weight: 600;
                margin-bottom: 0.3rem;
                margin-top: 0.3rem;
            }
            .icons{
                object-fit: contain;
                width: 25px;
                height: 25px;
                border-radius: 50%;
            }
            .graph img{
                object-fit: contain;
                width: 40px;
                height: 50px;
                transform: scale(2) rotateY(45deg);
            }
            .graph .dot{
                width: 12px;
                height: 12px;
                border-radius: 50%;
                border: 3px solid #fff;
                position: absolute;
                background-color: blue;
                box-shadow: 1px 1px 1px #a5a5a5;
                top: 25px;
            }
            .graph .dot:after{
                background-color: #fff;
                content: '$9,999.00';
                font-weight: 600;
                font-size: 0.7rem;
                position: absolute;
                top: -25px;
                left: -20px;
                box-shadow: 1px 1px 2px #a5a5a5;
                border-radius: 2px;
            }
            .font-weight-bold{
                font-size: 1.3rem;

            }
            #ethereum{
                transform: scale(2) rotateY(45deg) rotateX(180deg);
            }
            #ripple{
                transform: scale(2) rotateY(10deg) rotateX(20deg);
            }
            #eos{
                transform: scale(2) rotateY(50deg) rotateX(190deg);
            }



            /* utility classes */
            .table tr td{
                border: none;
            }
            .red{
                color: #ff2f2f;
                font-weight: 700;
            }
            .green{
                color: #1cbb1c;
                font-weight: 700;
            }
            .labels,.graph{
                position: relative;
            }
            .green-label{
                background-color: #00b300;
                color: #fff;
                font-weight: 600;
                font-size: 0.7rem;
            }
            .orange-label{
                background-color: #ffa500;
                color: #fff;
                font-weight: 600;
                font-size: 0.7rem;
            }
            .border-right{
                transform: scale(0.6);
                border-right: 1px solid black!important;
            }
            .box{
                transform: scale(1.5);
                background-color: #dbe2ff;
            }
            #top .table tbody tr{
                border-bottom: 1px solid #ddd;
            }
            #top .table tbody tr:last-child{
                border: none;
            }
            select{
                background-color: inherit;
                padding: 8px;
                border-radius: 5px;
                color: #444;
                border: 1px solid #444;
                outline-color: #00f;
            }
            .text-white{
                background-color: rgb(43, 159, 226);
                border-radius: 50%;
                font-size: 0.7rem;
                font-weight: 700;
                padding: 2px 3px;
            }
            a:hover{
                text-decoration: none;
            }
            a:hover .text-white{
                background-color: rgb(20, 92, 187);
            }

            /* Scrollbars */
            ::-webkit-scrollbar{
                width: 10px;
                height: 4px;
            }
            ::-webkit-scrollbar-thumb{
                background: linear-gradient(45deg,#999,#777);
                border-radius: 10px;

            }

            /* media Queries */
            @media(max-width:379px){
                .d-lg-flex .h3{
                    font-size: 1.4rem;
                }
            }
            @media(max-width:352px){
                #plat{
                    margin-top: 10px;
                }
            }


            .text-heading {
                margin-bottom: 30px;
                font-size: .8125rem;
            }
            .ratings{
                margin-right:10px;
            }

            .ratings i{

                color:#cecece;
                font-size:32px;
            }

            .rating-color{
                color:#fbc634 !important;
            }
            .small-ratings i{
                color:#cecece;   
            }
        </style>
    </head>

    <body>

        <section class="feature-area" style="margin-top: 1.5rem">

            <br>
            <br>
            <br>
            <div class="container mt-5">
                <div class="h3 text-muted">Statistic Mentee</div>
                <div id="starred" class="bg-white px-2 pt-1 mt-2">
                    <div class="table-responsive">
                        <table class="table">
                            <tbody>
                                <tr>
                                    <td>
                                        <div class="d-flex mt-2 border-right">
                                            <div class="box p-2 rounded">
                                                <span class="fas fa-star text-primary px-1"></span>
                                            </div>
                                        </div>
                                    </td>
                                    <td>
                                        <div class="d-flex flex-column">
                                            <div class="text-muted">Total Mentee</div>
                                            <div class="d-flex align-items-center">

                                                <b class="pl-2">${totalMentee}</b>
                                            </div>
                                        </div>
                                    </td>
                                    <td>
                                        <div class="d-flex flex-column">
                                            <div class="text-muted">Total hour of all request</div>
                                            <div><b>${totalH}</b></div>
                                        </div>
                                    </td>
                                    <td>
                                        <div class="d-flex flex-column">
                                            <div class="text-muted">Total skill of all request</div>
                                            <div><b>${totalS}</b></div>
                                        </div>
                                    </td>

                                </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
                <br>
                <br>
                <div class="h3 text-muted">List Mentee</div>
                <div id="top">
                    <div class="bg-white table-responsive">
                        <table class="table">
                            <thead>
                                <tr>
                                    <th>No.</th>
                                    <th>Name</th>
                                    <th>AccountName</th>
                                </tr>
                            </thead>
                            <tbody id="data">
                                <c:forEach items="${listMentee}" var="l" varStatus="i">
                                    <tr>
                                        <td>
                                            <div class="d-flex mt-2 border-right">
                                                <div class="box p-2 rounded">
                                                    <span class="text-primary px-2 font-weight-bold">${(i.index + 1)+(curP-1)*2}</span>
                                                </div>
                                            </div>
                                        </td>
                                        <td>
                                            <div class="d-flex flex-column">
                                                <div class="d-flex align-items-center">
                                                    <b class="pl-2">${l.name}</b>
                                                </div>
                                            </div>
                                        </td>
                                        <td>
                                            <div class="d-flex flex-column">
                                                <div><b>${l.username}</b></div>
                                            </div>
                                        </td>
                                    </tr>
                                </c:forEach>
                            </tbody>
                        </table>
                        <div style="text-align: right" id="page">
                            <c:if test="${curP!=1}"><button onclick="Pagination('${curP-1}', '${totalP}')" class="btn btn-outline-info">Previous</button> </c:if>
                            <c:forEach begin="${startP}" end="${endP}" varStatus="i">
                                <!--<a href="view?id=listMentor&page=${i.index}"   class="btn btn-outline-info  ${curP==i.index? "btn-outline-danger":""}">${i.index}</a>-->
                                <button  onclick="Pagination('${i.index}', '${totalP}')"  class="btn btn-outline-info  ${curP==i.index? "btn-outline-danger":""}">${i.index}</button>
                            </c:forEach>
                            <c:if test="${curP!=totalP}"><button onclick="Pagination('${curP+1}', '${totalP}')" class="btn btn-outline-info">Next</button> </c:if> 
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

</div>
<br>
<br>
<br>
<br>


</section>

<script src="js/vendor/jquery-2.2.4.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.11.0/umd/popper.min.js" integrity="sha384-b/U6ypiBEHpOf/4+1nzFpr53nxSS+GLCkfwBdFNTxtclqqenISfwAzpKaMNFNmj4"
crossorigin="anonymous"></script>
<script src="js/vendor/bootstrap.min.js"></script>
<script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBhOdIF3Y9382fqJYt5I_sswSrEw5eihAA"></script>
<script src="js/jquery.ajaxchimp.min.js"></script>
<script src="js/jquery.magnific-popup.min.js"></script>
<script src="js/parallax.min.js"></script>
<script src="js/owl.carousel.min.js"></script>
<script src="js/jquery.sticky.js"></script>
<script src="js/hexagons.min.js"></script>
<script src="js/jquery.counterup.min.js"></script>
<script src="js/waypoints.min.js"></script>
<script src="js/jquery.nice-select.min.js"></script>
<script src="js/main.js"></script>
</body>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.6.0/jquery.min.js"></script>
<script type="text/javascript">
                    function Page(index, totalP) {
                        $.ajax({
                            url: "/SWP391_HappyProgramming/admin",
                            type: "get", //send it through get method
                            data: {
                                action: 'listPageMentee',
                                index: index,
                                totalP: totalP
                            },
                            success: function (response) {
                                //Do Something
                                document.getElementById("page").innerHTML = response;

                            },
                            error: function (xhr) {
                                //Do Something to handle error
                            }
                        });
                    }

                    function Pagination(index, totalP) {
                        $.ajax({
                            url: "/SWP391_HappyProgramming/admin",
                            type: "get", //send it through get method
                            data: {
                                action: 'paginationListMentee',
                                index: index
                            },
                            success: function (response) {
                                //Do Something
                                document.getElementById("data").innerHTML = response;
                                Page(index, totalP);
                            },
                            error: function (xhr) {
                                //Do Something to handle error
                            }
                        });
                    }
</script>

</html>



