<%-- 
    Document   : fogotpass
    Created on : May 19, 2023, 11:27:11 AM
    Author     : PC
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Reset Password</title>
        <style>
            body{
                background-color: aliceblue;
            }
   .homepage--logo {
    width: 130px;
    height: auto;
    border-radius: 30%;
    display: flex;
    align-items:center;
    justify-content: center;
    margin: 0 auto    
   
}



.forgot-password {
    max-width: 700px;
    margin: 50px auto;
    width: 100%;
    
}

.forgot-password .row {
    margin: 0
}

.forgot-password .well {
    border-radius: 0;
}

.forgot-password .header {
    background: linear-gradient(29.81deg,#4d96ff 0%,#9570ee 100%);
    padding: 10px 15px;
    border: 1px solid #cccccc;
}


.forgot-password form {
    text-align: center;
        margin-top: 20px;
}

.forgot-password form .btn.pull-right {
    float: none !important
}

.bgr-warning {
    background: #fdfdfd;
        border: 1px solid #cccccc;
        padding: 20px;
        margin: 0 auto;
        max-width: 700px;
        font-size: 20px
}


.form-group{
    font-size: 20px;
    margin-bottom: 20px;
    border: 1px solid #cccccc;
}
.form-group a{
    font-size: 20px;
    margin-left: 20px
    }
input{
    width: 75%;
    border-radius: 4px;
    height: 25px
    }
button{
    font-size: 20px;
     margin-bottom: 15px;
    background-color: yellow;
    border-radius: 5px;
    padding: 6px
}
h4{
    color: red;
}

        </style>
    </head>
   <body>
  <div class="forgot-password">
    <div class="row">
      <div class="col-md-12 mx-auto text-center header">
        <a href="#">
          <img class="homepage--logo" src="image/logo.jpg">
        </a>
      </div>
    </div>
    <div class="well bgr-warning">
      <h3>Forgot your password?</h3>
      <p>Follow these simple steps to reset your account:</p>
      <ol>
        <li>Enter your username and email.</li>
        <li>Visit your email account, open the email sent by Web.</li>
        <li>Please change the password sent to your email before it expires.</li>
      </ol>
    </div>
    <form action="FogotServlet" method="post">
      <div class="form-group">
        <h3>Enter username and password</h3>
        <label for="username">Username:</label>
        <input type="text" id="username" name="user_name" required><br><br>
        
        <label for="email"> Email :&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</label>
        <input type="email" maxlength="128" id="email" name="email" required><br><br>
        <button type="submit">Get New Password</button>
         <a href="login.jsp">Login</a>
      <h4>${requestScope.su}</h4>
      <h4>${requestScope.err}</h4>
      </div>
      
    </form>
  </div>
</body>

</html>
