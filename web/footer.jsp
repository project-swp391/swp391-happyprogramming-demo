<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!doctype html>
<html lang="en">
  <head>
    <title>Title</title>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
    <link rel="stylesheet" href="./css/footer.css">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
  </head>
  <body>

    <footer id="footer" class="footer">

      <div class="container pt-5">
        <div class="row gy-4">
          <div class="col-lg-5 col-md-12 footer-info text-white">
            <a style="text-decoration: none;" href="index.html" class="logo d-flex align-items-center">
              <span style="font-size: 30px; color: aliceblue; padding-bottom: 10px;">E-Learning</span>
            </a>
            <p>Cras fermentum odio eu feugiat lide par naso tierra. Justo eget nada terra videa magna derita valies darta donna mare fermentum iaculis eu non diam phasellus.</p>
            <div class="social-links d-flex mt-4 ">
              <a href="https://www.facebook.com/quan.thaiba.524" class="facebook"><i class="fa fa-facebook-official" aria-hidden="true"></i></a>
              <a href="#" class="instagram"><i class="fa fa-instagram" aria-hidden="true"></i></a>
            </div>
          </div>
  
          <div class="col-lg-2 col-6 footer-links">
            <h4>Useful Links</h4>
            <ul>
              <li><a href="#">Home</a></li>
              <li><a href="#">About us</a></li>
              <li><a href="#">Services</a></li>
              <li><a href="#">Terms of service</a></li>
              <li><a href="#">Privacy policy</a></li>
            </ul>
          </div>
  
          <div class="col-lg-2 col-6 footer-links">
            <h4>Our Services</h4>
            <ul>
              <li><a href="#">Web Design</a></li>
              <li><a href="#">Web Development</a></li>
              <li><a href="#">Product Management</a></li>
              <li><a href="#">Marketing</a></li>
              <li><a href="#">Graphic Design</a></li>
            </ul>
          </div>
  
          <div class="col-lg-3 col-md-12 footer-contact  text-md-start">
            <h4>Contact Us</h4>
            <p>
              Khu Công nghệ cao Hòa Lạc,<br>
              Km29 Đại lộ Thăng Long,<br>
              huyện Thạch Thất, Hà Nội  <br><br>
              <strong>Phone:</strong> 0378793050<br>
              <strong>Email:</strong> quantbhe163724@fpt.edu.vn<br>
            </p>
  
          </div>
  
        </div>
      </div>
  
      <div class="container mt-4 pb-5">
        <div class="copyright text-white text-center">
          &copy;  Powered by  <strong><span>Quan</span></strong>. All Rights Reserved
        </div>
        <div class="credits text-white text-center">
         
          Designed by <a  href="https://www.facebook.com/quan.thaiba.524">Quantbhe163724</a>
        </div>
      </div>
  
    </footer><!-- End Footer -->
    <!-- Optional JavaScript -->
    <!-- jQuery first, then Popper.js, then Bootstrap JS -->
    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>
  </body>
</html>