
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <title>Happy Programming Academy - Skills</title>
  <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/css/bootstrap.min.css">
</head>
<style>
    
</style>

<body>
    <jsp:include page="menu.jsp"></jsp:include>
  <div class="container mt-5">
    <table class="table" style="box-shadow: 0 2px 5px rgba(0, 0, 0, 0.1);">
        <a href="createskill.jsp" class="btn btn-success" style="margin-bottom: 15px;margin-left: 30px ;float: right;" >+ Add New Skill</a><br><!-- comment -->
      <thead class="thead-dark">
        
        <tr>
          <th scope="col">STT</th>
          <th scope="col">ID</th>
          <th scope="col">Name of Skill</th>
          <th scope="col">Status</th>
          <th scope="col">Actions</th>
        </tr>
      </thead>
      <tbody>
          <c:forEach var="o" items="${ListS}" varStatus="loop" begin="1" end="${ListS.size()}">
        <tr>
          <th scope="row"><c:out value="${loop.index}"/></th>
          <td>${o.id}</td>
          <td>${o.name}</td>
          <c:if test="${o.status == 'active'}">
          <td>
              <button onclick="window.location.href = 'update-status-skill?sid=${o.id}&status=${o.status}' " class="btn btn-success">Enabled</button>
          </td>
          </c:if>
          <c:if test="${o.status == 'inactive'}">
              <td>
                  <button onclick="window.location.href ='update-status-skill?sid=${o.id}&status=${o.status}' " class="btn btn-secondary">Disabled</button>
              </td>
          </c:if>
          <td><a href="updateSkill?id=${o.id}" class="btn btn-primary">Update Skill</a></td>
        </tr>
        </c:forEach>
        
      </tbody>
    </table>
  </div>

  <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/js/bootstrap.min.js"></script>
</body>

</html>