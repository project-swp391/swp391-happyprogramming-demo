
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!doctype html>
<html lang="en">

    <head>
        <title>Update Mentor</title>
        <!-- Required meta tags -->
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

        <!-- Bootstrap CSS -->
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.15.3/css/all.min.css">
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css"

              integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
    </head>

    <body>
        <jsp:include page="menu.jsp"></jsp:include>
            <style>
                section{
                    margin-bottom: 30px;
                    margin-top: 20px ;
                }
                ul.list.menuList {
                    list-style: none;
                }

                ul.list.menuList li:before {
                    content: "";
                    display: none;
                }

                ul.list.menuList li {
                    list-style: none;
                }

                #avatarThumb {
                    position: relative;
                }

                #avatarThumb .btn-change-avatar {
                    position: absolute;
                    top: 0;
                    left: 0;
                    background: rgba(0, 0, 0, 0.4);
                    color: #ccc;
                    border-radius: 0;
                }

                #avatarThumb:hover .btn-change-avatar {

                    background: rgba(0, 0, 0, 0.8);
                    color: white;

                }
                .regis-mentor{
                    text-align: center;
                }
            </style>

            <section ng-app="mainApp" id="mainAppContent" class="ng-scope">
                <div class="container ng-scope" ng-controller="mainCtroller">
                    <div class="section-content">
                        <div class="row">
                            <div class="col-sx-12 col-sm-3 col-md-3" style="background-color: #FFE4C4;
                                 height: 800px;
                                 padding-top: 20px;
                                 margin-right: auto;">

                                <div class="doctor-thumb" id="avatarThumb" style="background-color: bisque;padding-left: 15px;padding-top: 10px;margin-bottom: 20px;">

                                    <img class="avatarImage"
                                         src="${p.avatar}"
                                    alt="" style="width:95%;height: auto;" ">
                                <div class="avatarEditor croppie-container" id="avatarThumbCroppie" style="display:none;">
                                    <div class="cr-boundary" aria-dropeffect="none" style="width: 263px; height: 263px;">
                                        <img class="cr-image" alt="preview" aria-grabbed="false">
                                        <div class="cr-viewport cr-vp-square" tabindex="0"
                                             style="width: 263px; height: 263px;"></div>
                                        <div class="cr-overlay"></div>
                                    </div>
                                    <div class="cr-slider-wrap"><input class="cr-slider" type="range" step="0.0001"
                                                                       aria-label="zoom"></div>
                                </div>

                                <form action="upimage" method="post" enctype="multipart/form-data">
                                    <div class="text-center btnSave mb-10" style="display: grid;">
                                        <input type="file"  name="fileimage" style="width: 95% ;">
                                        <input class="btn btn-xs btn-dark" type="submit" value="cập nhật" style="width: 95% ;">


                                    </div>
                                </form>  
                                <hr/>
                            </div>

                            <div class="info p-20 bg-black-333" style="background-color: bisque;">
                                <ul class="list angle-double-right m-0">
                                    <li class="mt-0 text-gray-silver"><strong class="text-gray-lighter">Username: </strong> <b
                                            style="color:green;">${sessionScope.acc.user_name}</b></li>

                                </ul>

                                <ul class="list list-divider list-border menuList">
                                    <li><a href="/thong-tin-ca-nhan"><i
                                                class="far fa-address-card mr-10 text-black-light"></i> Thông tin cá nhân</a>
                                        <p>---------------------------
                                    </li>
                                    <li><a href="/thong-tin-dang-nhap"><i
                                                class="far fa-user-circle mr-10 text-black-light"></i> Thông tin đăng nhập</a>
                                        <p>---------------------------
                                    </li>
                                    <li><a href="/danh-sach-lop-day"><i class="far fa-list-alt mr-10 text-black-light"></i> Danh sách lớp dạy</a>
                                        <p>---------------------------
                                    </li>
                                    <li><a href="/danh-sach-lop-hoc"><i class="far fa-list-alt mr-10 text-black-light"></i> Danh sách lớp học</a>
                                        <p>---------------------------
                                    </li>

                                    <li><a href="logout"><i class="fas fa-sign-out-alt mr-10 text-black-light"></i> Đăng xuất</a>
                                        <p>---------------------------
                                    </li>
                                </ul>
                            </div>

                        </div>
                        <div class="col-xs-12 col-sm-9 col-md-9">
                            <div>

                                <form name="registmentor" method="post" class="ng-pristine ng-valid ng-valid-email">
                                    <h3 class="line-bottom mt-0">Personal Information</h3>
                                    <div class="row">
                                        <div class="form-group col-md-6">
                                            <label>Fullname</label>
                                            <input class="form-control ng-pristine ng-untouched ng-valid ng-not-empty"
                                                   type="text" name="fullname" value="${p.fullname}">
                                        </div>
                                        <div class="form-group col-md-6">
                                            <label>Phone</label>
                                            <input class="form-control ng-pristine ng-untouched ng-valid ng-empty"
                                                   type="phone" name="phone" value="${p.phone}" >
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="form-group col-md-5">
                                            <label>Email</label>
                                            <input
                                                class="form-control ng-pristine ng-valid ng-empty ng-valid-email ng-touched"
                                                type="email" name="email" value="${sessionScope.acc.email}" readonly>
                                        </div>


                                        <div class="form-group col-md-3">
                                            <label>Date of birth</label><br>
                                            <input class="form-control ng-pristine ng-untouched ng-valid ng-empty"
                                                   type="date" name="dob" value="${p.dob}" >
                                        </div>
                                        <div class="form-group col-md-4" >
                                            <label>Gender</label><br>
                                            <c:if test="${p.sex == false}">
                                                <select name="gender" style="height: 38px;" >
                                                    <option selected="" value="0" >Male</option>
                                                    <option value="1" >Female</option>
                                                </select>
                                            </c:if>
                                            <c:if test="${p.sex == true}">
                                                <select name="gender" style="height: 38px;" >
                                                    <option  value="0" >Male</option>
                                                    <option selected="" value="1" >Female</option>
                                                </select>
                                            </c:if>

                                        </div>

                                    </div>
                                    <div class="row">
                                        <div class="form-group col-md-12">
                                            <label>Address</label>
                                            <input class="form-control ng-pristine ng-untouched ng-valid ng-empty"
                                                   type="text" value="${p.address}" name="address">
                                        </div>

                                    </div>

                                    <div class="row">
                                        <div class="form-group col-md-12">
                                            <label>Introduce Yourself<span
                                                    class="ng-binding">(1500 character)</span></label>
                                            <textarea name="description"
                                                      class="form-control ng-pristine ng-valid ng-empty ng-touched" cols="20" 
                                                      rows="5"  id="giasu">${p.description}</textarea>
                                        </div>
                                    </div>
                                    <h3 class="line-bottom">Information Mentor</h3>

                                    <div class="row">
                                        <div class="form-group col-md-6">
                                            <label> Profession:
                                                <i class="fas fa-star-of-life text-danger"></i>
                                            </label>
                                            <select class="form-group col-md-12" name="job" style="height: 40px;" >

                                                <option  value="0"></option>
                                                <option  value="1"  >- Student -</option>
                                                <option  value="2"  >- Teacher -</option>
                                                <option  value="3"  >- Graduated -</option>
                                            </select>
                                        </div>

                                    </div>
                                    <div class="row">
                                        <div class="form-group col-md-12">
                                            <label>Profession Introduction: </label>
                                            <textarea name="intro_job"
                                                      class="form-control ng-pristine ng-valid ng-empty ng-touched" cols="20"
                                                      rows="3"  ></textarea>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="form-group col-md-12">
                                            <label>Skill Training:
                                                <i class="fas fa-star-of-life text-danger"></i>
                                            </label><br>
                                            <c:forEach var="o" items="${ListS}">
                                                <div class="form-group col-md-3" style="display: inline-table">
                                                    <input type="checkbox"  name="skill" value="${o.id}"> ${o.name} <br>
                                                </div>
                                            </c:forEach>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="form-group col-md-12">
                                            <label>Service Description: </label>
                                            <textarea name="service"
                                                      class="form-control ng-pristine ng-valid ng-empty ng-touched" cols="20"
                                                      rows="3"  ></textarea>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="form-group col-md-12">
                                            <label>Achievement Description: </label>
                                            <textarea name="achievement"
                                                      class="form-control ng-pristine ng-valid ng-empty ng-touched" cols="20"
                                                      rows="3"  ></textarea>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="form-group col-md-12">
                                            <label>Famework Training:
                                                <i class="fas fa-star-of-life text-danger"></i>
                                            </label><br>
                                            <c:forEach var="o" items="${ListF}">
                                                <div class="form-group col-md-3" style="display: inline-table">
                                                    <input type="checkbox"  name="famework" value="${o.fameID}"> ${o.fameName} <br>
                                                </div>
                                            </c:forEach>
                                        </div>
                                    </div>
                                    <div class="regis-mentor" style="margin-bottom: 15px;">
                                        <input class="btn btn-success btn-lg mt-15" type="submit" value="Register Mentor" />
                                    </div>
                                </form>
                            </div>
                        </div>

                    </div>
                </div>

            </div>
        </section>


        <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js"
                integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo"
        crossorigin="anonymous"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js"
                integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1"
        crossorigin="anonymous"></script>
        <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"
                integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM"
        crossorigin="anonymous"></script>
        <jsp:include page="footer.jsp" ></jsp:include>
    </body>

</html>
