<%-- 
    Document   : home1
    Created on : May 22, 2023, 4:16:05 PM
    Author     : quanr
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!doctype html>
<html lang="en">

<head>
    <title>Home</title>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css"
        integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
    <link rel="stylesheet" href="./css/home1.css">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">

</head>

<body>
    <jsp:include page="menu.jsp"></jsp:include>
<div class="page-content bg-white">
        <div class="section-area section-sp1 ovpr-dark bg-fix online-cours">
            <div class="background-color-tim background-header-1">
                <div class="container">
                    <div class="row">
                        <div class="col-md-12 text-center text-white pt-5 pb-5">
                            <h2>Online Courses To Learn</h2>
                            <h5>Own Your Feature Learning New Skills Online</h5>
                            <form class="cours-search pt-4 ">
                                <div class="input-group" style="width: 50%; margin: auto;">
                                    <input type="text" class="form-control" placeholder="What do you want to learn today?	">
                                    <div class="input-group-append">
                                        <button class="btn bg-warning" type="submit">Search</button>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                    <div class="mw800 m-auto">
                        <div class="row">
                            <div class="col-md-4 col-sm-6">
                                <div class="cours-search-bx m-b30 " style="height: 150px;">
                                    <div class="icon-box-1">
                                        <div class="icon-box">
                                            <h3><span
                                                    class="counter">5</span>M</h3>
                                        </div>
                                        <span class="cours-search-text">Over 5 million student</span>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-4 col-sm-6">
                                <div class="cours-search-bx m-b30">
                                    <div class="icon-box-1">
                                        <div class="icon-box">
                                            <h3><span
                                                    class="counter">30</span>K</h3>
                                        </div>
                                        <span class="cours-search-text">30,000 Courses.</span>
                                    </div>
    
                                </div>
                            </div>
                            <div class="col-md-4 col-sm-12">
                                <div class="cours-search-bx m-b30">
                                    <div class="icon-box-1">
                                        <div class="icon-box">
                                            <h3><span
                                                    class="counter">20</span>K</h3>
                                        </div>
                                        <span class="cours-search-text">Learn Anythink Online.</span>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <!-- Main slider -->
        <div class="content-block pt-5">
            <!-- Popular Courses -->
            <div class="section-area section-sp2 popular-courses-bx">
                <div class="container">
                    <div class="row pb-5">
                        <div class="col-md-12 heading-bx left">
                            <h2 class="title-head">Popular<span>Courses</span></h2>
                            <p style="margin-top: 10px;">It is a long established fact that a reader will be distracted
                                by the readable content of a page</p>
                        </div>
                    </div>
                    <div id="carouselId" class="carousel slide p-3 bg-light" data-ride="carousel"
                        style="border-radius: 5px;">
                        <ol class="carousel-indicators">
                            <li data-target="#carouselId" data-slide-to="0" class="active"></li>
                            <li data-target="#carouselId" data-slide-to="1"></li>
                        <li data-target="#carouselId" data-slide-to="2"></li>
                        </ol>
                        <div class="carousel-inner" role="listbox">
                            <div class="carousel-item active">
                                <div class="row">
                                    <div class="item col-md-3">
                                        <div class="cours-bx">
                                            <div class="action-box">
                                                <img src="./image/course/pic1.jpg" alt="">
                                                <a href="#" class="btn">Read More</a>
                                            </div>
                                            <div class="info-bx text-center">
                                                <h5><a href="#">Introduction EduChamp – LMS plugin</a></h5>
                                                <span>Programming</span>
                                            </div>
                                            <div class="cours-more-info">
                                                <div class="review">
                                                    <span>3 Review</span>
                                                    <ul class="cours-star ">
                                                        <li class="active"><i class="fa fa-star"></i></li>
                                                        <li class="active"><i class="fa fa-star"></i></li>
                                                        <li class="active"><i class="fa fa-star"></i></li>
                                                        <li><i class="fa fa-star"></i></li>
                                                        <li><i class="fa fa-star"></i></li>
                                                    </ul>
                                                </div>
                                                <div class="price text-center">
                                                    
                                                        <del>$190</del>
                                                        <h5>$120</h5>
                                                    
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="item col-md-3">
                                        <div class="cours-bx">
                                            <div class="action-box">
                                                <img src="./image/course/pic1.jpg" alt="">
                                                <a href="#" class="btn">Read More</a>
                                            </div>
                                            <div class="info-bx text-center">
                                                <h5><a href="#">Introduction EduChamp – LMS plugin</a></h5>
                                                <span>Programming</span>
                                            </div>
                                            <div class="cours-more-info">
                                                <div class="review">
                                                    <span>3 Review</span>
                                                    <ul class="cours-star">
                                                        <li class="active"><i class="fa fa-star"></i></li>
                                                        <li class="active"><i class="fa fa-star"></i></li>
                                                        <li class="active"><i class="fa fa-star"></i></li>
                                                        <li><i class="fa fa-star"></i></li>
                                                        <li><i class="fa fa-star"></i></li>
                                                    </ul>
                                                </div>
                                                <div class="price">
                                                    <del>$190</del>
                                                    <h5>$120</h5>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="item col-md-3">
                                        <div class="cours-bx">
                                            <div class="action-box">
                                                <img src="./image/course/pic1.jpg" alt="">
                                                <a href="#" class="btn">Read More</a>
                                            </div>
                                            <div class="info-bx text-center">
                                                <h5><a href="#">Introduction EduChamp – LMS plugin</a></h5>
                                                <span>Programming</span>
                                            </div>
                                            <div class="cours-more-info">
                                                <div class="review">
                                                    <span>3 Review</span>
                                                    <ul class="cours-star">
                                                        <li class="active"><i class="fa fa-star"></i></li>
                                                        <li class="active"><i class="fa fa-star"></i></li>
                                                        <li class="active"><i class="fa fa-star"></i></li>
                                                        <li><i class="fa fa-star"></i></li>
                                                        <li><i class="fa fa-star"></i></li>
                                                    </ul>
                                                </div>
                                                <div class="price">
                                                    <del>$190</del>
                                                    <h5>$120</h5>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="item col-md-3">
                                        <div class="cours-bx">
                                            <div class="action-box">
                                                <img src="./image/course/pic1.jpg" alt="">
                                                <a href="#" class="btn">Read More</a>
                                            </div>
                                            <div class="info-bx text-center">
                                                <h5><a href="#">Introduction EduChamp – LMS plugin</a></h5>
                                                <span>Programming</span>
                                            </div>
                                            <div class="cours-more-info">
                                                <div class="review">
                                                    <span>3 Review</span>
                                                    <ul class="cours-star">
                                                        <li class="active"><i class="fa fa-star"></i></li>
                                                        <li class="active"><i class="fa fa-star"></i></li>
                                                        <li class="active"><i class="fa fa-star"></i></li>
                                                        <li><i class="fa fa-star"></i></li>
                                                        <li><i class="fa fa-star"></i></li>
                                                    </ul>
                                                </div>
                                                <div class="price">
                                                    <del>$190</del>
                                                    <h5>$120</h5>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="carousel-item">
                                <div class="row">
                                    <div class="item col-md-3">
                                        <div class="cours-bx">
                                            <div class="action-box">
                                                <img src="./image/course/pic1.jpg" alt="">
                                                <a href="#" class="btn">Read More</a>
                                            </div>
                                            <div class="info-bx text-center">
                                                <h5><a href="#">Introduction EduChamp – LMS plugin</a></h5>
                                                <span>Programming</span>
                                            </div>
                                            <div class="cours-more-info">
                                                <div class="review">
                                                    <span>3 Review</span>
                                                    <ul class="cours-star ">
                                                        <li class="active"><i class="fa fa-star"></i></li>
                                                        <li class="active"><i class="fa fa-star"></i></li>
                                                        <li class="active"><i class="fa fa-star"></i></li>
                                                        <li><i class="fa fa-star"></i></li>
                                                        <li><i class="fa fa-star"></i></li>
                                                    </ul>
                                                </div>
                                                <div class="price text-center">
                                                    
                                                        <del>$190</del>
                                                        <h5>$120</h5>
                                                    
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="item col-md-3">
                                        <div class="cours-bx">
                                            <div class="action-box">
                                                <img src="./image/course/pic1.jpg" alt="">
                                                <a href="#" class="btn">Read More</a>
                                            </div>
                                            <div class="info-bx text-center">
                                                <h5><a href="#">Introduction EduChamp – LMS plugin</a></h5>
                                                <span>Programming</span>
                                            </div>
                                            <div class="cours-more-info">
                                                <div class="review">
                                                    <span>3 Review</span>
                                                    <ul class="cours-star">
                                                        <li class="active"><i class="fa fa-star"></i></li>
                                                        <li class="active"><i class="fa fa-star"></i></li>
                                                        <li class="active"><i class="fa fa-star"></i></li>
                                                        <li><i class="fa fa-star"></i></li>
                                                        <li><i class="fa fa-star"></i></li>
                                                    </ul>
                                                </div>
                                                <div class="price">
                                                    <del>$190</del>
                                                    <h5>$120</h5>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="item col-md-3">
                                        <div class="cours-bx">
                                            <div class="action-box">
                                                <img src="./image/course/pic1.jpg" alt="">
                                                <a href="#" class="btn">Read More</a>
                                            </div>
                                            <div class="info-bx text-center">
                                                <h5><a href="#">Introduction EduChamp – LMS plugin</a></h5>
                                                <span>Programming</span>
                                            </div>
                                            <div class="cours-more-info">
                                                <div class="review">
                                                    <span>3 Review</span>
                                                    <ul class="cours-star">
                                                        <li class="active"><i class="fa fa-star"></i></li>
                                                        <li class="active"><i class="fa fa-star"></i></li>
                                                        <li class="active"><i class="fa fa-star"></i></li>
                                                        <li><i class="fa fa-star"></i></li>
                                                        <li><i class="fa fa-star"></i></li>
                                                    </ul>
                                                </div>
                                                <div class="price">
                                                    <del>$190</del>
                                                    <h5>$120</h5>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="item col-md-3">
                                        <div class="cours-bx">
                                            <div class="action-box">
                                                <img src="./image/course/pic1.jpg" alt="">
                                                <a href="#" class="btn">Read More</a>
                                            </div>
                                            <div class="info-bx text-center">
                                                <h5><a href="#">Introduction EduChamp – LMS plugin</a></h5>
                                                <span>Programming</span>
                                            </div>
                                            <div class="cours-more-info">
                                                <div class="review">
                                                    <span>3 Review</span>
                                                    <ul class="cours-star">
                                                        <li class="active"><i class="fa fa-star"></i></li>
                                                        <li class="active"><i class="fa fa-star"></i></li>
                                                        <li class="active"><i class="fa fa-star"></i></li>
                                                        <li><i class="fa fa-star"></i></li>
                                                        <li><i class="fa fa-star"></i></li>
                                                    </ul>
                                                </div>
                                                <div class="price">
                                                    <del>$190</del>
                                                    <h5>$120</h5>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                        </div>
                        <div class="carousel-item">
                            <div class="row">
                                <div class="item col-md-3">
                                    <div class="cours-bx">
                                        <div class="action-box">
                                            <img src="./image/course/pic1.jpg" alt="">
                                            <a href="#" class="btn">Read More</a>
                                        </div>
                                        <div class="info-bx text-center">
                                            <h5><a href="#">Introduction EduChamp – LMS plugin</a></h5>
                                            <span>Programming</span>
                                        </div>
                                        <div class="cours-more-info">
                                            <div class="review">
                                                <span>3 Review</span>
                                                <ul class="cours-star ">
                                                    <li class="active"><i class="fa fa-star"></i></li>
                                                    <li class="active"><i class="fa fa-star"></i></li>
                                                    <li class="active"><i class="fa fa-star"></i></li>
                                                    <li><i class="fa fa-star"></i></li>
                                                    <li><i class="fa fa-star"></i></li>
                                                </ul>
                                            </div>
                                            <div class="price text-center">
                                                
                                                    <del>$190</del>
                                                    <h5>$120</h5>
                                                
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="item col-md-3">
                                    <div class="cours-bx">
                                        <div class="action-box">
                                            <img src="./image/course/pic1.jpg" alt="">
                                            <a href="#" class="btn">Read More</a>
                                        </div>
                                        <div class="info-bx text-center">
                                            <h5><a href="#">Introduction EduChamp – LMS plugin</a></h5>
                                            <span>Programming</span>
                                        </div>
                                        <div class="cours-more-info">
                                            <div class="review">
                                                <span>3 Review</span>
                                                <ul class="cours-star">
                                                    <li class="active"><i class="fa fa-star"></i></li>
                                                    <li class="active"><i class="fa fa-star"></i></li>
                                                    <li class="active"><i class="fa fa-star"></i></li>
                                                    <li><i class="fa fa-star"></i></li>
                                                    <li><i class="fa fa-star"></i></li>
                                                </ul>
                                            </div>
                                            <div class="price">
                                                <del>$190</del>
                                                <h5>$120</h5>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="item col-md-3">
                                    <div class="cours-bx">
                                        <div class="action-box">
                                            <img src="./image/course/pic1.jpg" alt="">
                                            <a href="#" class="btn">Read More</a>
                                        </div>
                                        <div class="info-bx text-center">
                                            <h5><a href="#">Introduction EduChamp – LMS plugin</a></h5>
                                            <span>Programming</span>
                                        </div>
                                        <div class="cours-more-info">
                                            <div class="review">
                                                <span>3 Review</span>
                                                <ul class="cours-star">
                                                    <li class="active"><i class="fa fa-star"></i></li>
                                                    <li class="active"><i class="fa fa-star"></i></li>
                                                    <li class="active"><i class="fa fa-star"></i></li>
                                                    <li><i class="fa fa-star"></i></li>
                                                    <li><i class="fa fa-star"></i></li>
                                                </ul>
                                            </div>
                                            <div class="price">
                                                <del>$190</del>
                                                <h5>$120</h5>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="item col-md-3">
                                    <div class="cours-bx">
                                        <div class="action-box">
                                            <img src="./image/course/pic1.jpg" alt="">
                                            <a href="#" class="btn">Read More</a>
                                        </div>
                                        <div class="info-bx text-center">
                                            <h5><a href="#">Introduction EduChamp – LMS plugin</a></h5>
                                            <span>Programming</span>
                                        </div>
                                        <div class="cours-more-info">
                                            <div class="review">
                                                <span>3 Review</span>
                                                <ul class="cours-star">
                                                    <li class="active"><i class="fa fa-star"></i></li>
                                                    <li class="active"><i class="fa fa-star"></i></li>
                                                    <li class="active"><i class="fa fa-star"></i></li>
                                                    <li><i class="fa fa-star"></i></li>
                                                    <li><i class="fa fa-star"></i></li>
                                                </ul>
                                            </div>
                                            <div class="price">
                                                <del>$190</del>
                                                <h5>$120</h5>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        </div>
                        <a style="width: 50px"  class="carousel-control-prev" href="#carouselId" role="button" data-slide="prev">
                            <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                            
                            <span class="sr-only">Previous</span>
                        </a>
                        <a style="width: 50px" class="carousel-control-next"  href="#carouselId" role="button" data-slide="next">
                            <span class="carousel-control-next-icon " aria-hidden="true"></span>
                            
                            <span class="sr-only">Next</span>
                        </a>
                    </div>
                </div>
            </div>
        </div>


        <!-- Popular Courses END -->
        <div class="content-block">
            <div class="section-area section-sp2 bg-fix ovbl-dark join-bx text-center"
                style="background-image:url(image/bg1.jpg);">
                <div class="background-color-tim">
                    <div class="container">
                        <div class="row">
                            <div class="col-md-12">
                                <div class="join-content-bx text-white" style="padding: 80px 0px;">
                                    <h2>Learn a new skill online on <br> your time</h2>
                                    <h4 style="padding: 10px 0px;"><span class="counter">57,000</span> Online Courses
                                    </h4>
                                    <p style=" margin: auto; padding-top: 60px;">Lorem Ipsum is simply dummy text of the
                                        printing and typesetting industry. Lorem Ipsum has been the industry's standard
                                        dummy text ever since the 1500s, when an unknown printer took a galley of type
                                        and scrambled it to make a type specimen book.Lorem Ipsum is simply dummy text
                                        of the printing and typesetting industry. Lorem Ipsum has been the industry's
                                        standard dummy text ever since the 1500s, when an unknown printer took a galley
                                        of type and scrambled it to make a type specimen book.</p>
                                    <div class="join-now-course">
                                        <a style="color: aliceblue;" href="#" class="btn button-md join-now-btn">Join
                                            Now</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <!-- Form END -->
        <div class="section-area section-sp bg-light">
            <div class="container">
                <div class="row" style="padding: 70px 0px;">
                    <div class="col-lg-6 m-b30 mt-4">
                        <h2 class="title-head ">Learn a new skill online<br> <span class="text-primary"> on your
                                time</span></h2>
                        <h4 style="padding: 30px 0px;"><span class="counter">57,000</span> Online Courses</h4>
                        <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has
                            been the industry's standard dummy text ever since the 1500s, when an unknown printer took a
                            galley of type.</p>
                        <a href="#" class="btn button-md join-course-now-2">Join Now</a>
                    </div>
                    <div class="col-lg-6">
                        <div class="row">
                            <div class="col-lg-6 col-md-6 col-sm-6 m-b30">
                                <div class="feature-container">
                                    <div class="feature-md text-white m-b20">
                                        <a href="#" class="icon-cell"><img src="./images/1.png" alt=""></a>
                                    </div>
                                    <div class="icon-content">
                                        <h5 class="ttr-tilte">Our Philosophy</h5>
                                        <p>Lorem ipsum dolor sit amet, consectetuer adipiscing.</p>
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-6 col-md-6 col-sm-6 m-b30">
                                <div class="feature-container">
                                    <div class="feature-md text-white m-b20">
                                        <a href="#" class="icon-cell"><img src="../images/3.png" alt=""></a>
                                    </div>
                                    <div class="icon-content">
                                        <h5 class="ttr-tilte">Kingster's Principle</h5>
                                        <p>Lorem ipsum dolor sit amet, consectetuer adipiscing.</p>
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-6 col-md-6 col-sm-6 m-b30">
                                <div class="feature-container">
                                    <div class="feature-md text-white m-b20">
                                        <a href="#" class="icon-cell"><img src="./images/4.png" alt=""></a>
                                    </div>
                                    <div class="icon-content">
                                        <h5 class="ttr-tilte">Key Of Success</h5>
                                        <p>Lorem ipsum dolor sit amet, consectetuer adipiscing.</p>
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-6 col-md-6 col-sm-6 m-b30">
                                <div class="feature-container">
                                    <div class="feature-md text-white m-b20">
                                        <a href="#" class="icon-cell"><img src="./images/2.png" alt=""></a>
                                    </div>
                                    <div class="icon-content">
                                        <h5 class="ttr-tilte">Our Philosophy</h5>
                                        <p>Lorem ipsum dolor sit amet, consectetuer adipiscing.</p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <!-- Testimonials -->
        <div class="section-area section-5 bg-fix text-white" style="background-image:url(../images/bg1.jpg);">
            <div class="background-color-tim">
                <div class="container">
                    <div class="row">
                        <div class="col-lg-3 col-md-6 col-sm-6 col-6 m-b30">
                            <div class="counter-style-1">
                                <div class="text-white">
                                    <span class="counter">3000</span><span class="cong">+</span>
                                </div>
                                <span class="counter-text">Completed Projects</span>
                            </div>
                        </div>
                        <div class="col-lg-3 col-md-6 col-sm-6 col-6 m-b30">
                            <div class="counter-style-1">
                                <div class="text-white">
                                    <span class="counter">2500</span><span class="cong">+</span>
                                </div>
                                <span class="counter-text">Happy Clients</span>
                            </div>
                        </div>
                        <div class="col-lg-3 col-md-6 col-sm-6 col-6 m-b30">
                            <div class="counter-style-1">
                                <div class="text-white">
                                    <span class="counter">1500</span><span class="cong">+</span>
                                </div>
                                <span class="counter-text">Questions Answered</span>
                            </div>
                        </div>
                        <div class="col-lg-3 col-md-6 col-sm-6 col-6 m-b30">
                            <div class="counter-style-1">
                                <div class="text-white">
                                    <span class="counter">1000</span><span class="cong">+</span>
                                </div>
                                <span class="counter-text">Ordered Coffee's</span>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- Testimonials END -->

        <!-- Lớp học nổi bật -->
        <section>
            <div class="container" style="padding: 70px 0px;">
                <div class="row">
                    <div class="col-md-12">
                        <div class="row">
                            <div class="container ">
                                <div class="row">
                                    <div class="col-md-3">
                                        <div class="widget">
                                            <h2 class="widget-title line-bottom-theme-colored-2 ">Gia sư <b
                                                    class="text-theme-colored2">Mới</b></h2>
                                            <ul class="angle-double-right list-border">
                                                <li><a target="_blank" href="/gia-su/trinh-thi-kim-chi-45064"><img
                                                            style="width:24px;"
                                                            src="https://www.daykemtainha.vn/public/files/avatar_crop_48_48/45064_avatar.jpeg"
                                                            alt="Trịnh Thị Kim Chi" class="img-circle mr-10"> Trịnh Thị Kim
                                                        Chi</a> </li>
                                                <li><a target="_blank" href="/gia-su/nguyen-thi-tam-45043"><img
                                                            style="width:24px;"
                                                            src="https://www.daykemtainha.vn/public/files/avatar_crop_48_48/45043_avatar.jpeg"
                                                            alt="Nguyễn Thị Tâm" class="img-circle mr-10"> Nguyễn Thị
                                                        Tâm</a> </li>
                                                <li><a target="_blank" href="/gia-su/dong-ho-anh-minh-44957"><img
                                                            style="width:24px;"
                                                            src="https://www.daykemtainha.vn/public/files/avatar_crop_48_48/44957_avatar.jpeg"
                                                            alt="Đồng Hồ Ánh Minh" class="img-circle mr-10"> Đồng Hồ Ánh
                                                        Minh</a> </li>
                                                <li><a target="_blank" href="/gia-su/phung-thi-diep-44937"><img
                                                            style="width:24px;"
                                                            src="https://www.daykemtainha.vn/public/files/avatar_crop_48_48/44937_avatar.jpg"
                                                            alt="Phùng Thị Điệp" class="img-circle mr-10"> Phùng Thị
                                                        Điệp</a> </li>
                                                <li><a target="_blank" href="/gia-su/nguyen-thi-dieu-linh-44795"><img
                                                            style="width:24px;"
                                                            src="https://www.daykemtainha.vn/public/files/avatar_crop_48_48/44795_avatar.jpg"
                                                            alt="Nguyễn Thị diệu Linh" class="img-circle mr-10"> Nguyễn Thị
                                                        diệu Linh</a> </li>
                                                <li><a target="_blank" href="/gia-su/bao-ngoc-44787"><img
                                                            style="width:24px;"
                                                            src="https://www.daykemtainha.vn/public/files/avatar_crop_48_48/44787_avatar.jpeg"
                                                            alt="Bảo Ngọc" class="img-circle mr-10"> Bảo Ngọc</a> </li>
                                                <li><a target="_blank" href="/gia-su/thanh-ngan-44785"><img
                                                            style="width:24px;"
                                                            src="https://www.daykemtainha.vn/public/files/avatar_crop_48_48/44785_avatar.jpeg"
                                                            alt="Thanh Ngân" class="img-circle mr-10"> Thanh Ngân</a> </li>
                                                <li><a target="_blank" href="/gia-su/pham-tran-mai-duyen-44780"><img
                                                            style="width:24px;"
                                                            src="https://www.daykemtainha.vn/public/files/avatar_crop_48_48/44780_avatar.jpeg"
                                                            alt="Phạm Trần Mai Duyên" class="img-circle mr-10"> Phạm Trần
                                                        Mai Duyên</a> </li>
                                                <li><a target="_blank" href="/gia-su/cao-thi-yen-nhi-44779"><img
                                                            style="width:24px;"
                                                            src="https://www.daykemtainha.vn/public/files/avatar_crop_48_48/44779_avatar.jpeg"
                                                            alt="Cao Thị Yến Nhi" class="img-circle mr-10"> Cao Thị Yến
                                                        Nhi</a> </li>
                                                <li><a target="_blank" href="/gia-su/tran-thuy-linh-44778"><img
                                                            style="width:24px;"
                                                            src="https://www.daykemtainha.vn/public/files/avatar_crop_48_48/44778_avatar.jpg"
                                                            alt="Trần Thuỳ Linh" class="img-circle mr-10"> Trần Thuỳ
                                                        Linh</a> </li>
    
                                            </ul>
    
                                        </div>
                                    </div>
                                    <div class="col-md-3">
                                        <div class="widget">
                                            <h2 class="widget-title line-bottom-theme-colored-2 ">Gia sư nổi bật<b
                                                    class="text-theme-colored2"> Tháng 05</b></h2>
                                            <ul class="angle-double-right list-border">
                                                <li><a target="_blank" href="/gia-su/pham-thi-thom-2946"><img
                                                            style="width:24px;"
                                                            src="https://www.daykemtainha.vn/public/files/avatar_crop_48_48/2946_avatar.jpg"
                                                            alt="Phạm Thị Thơm" class="img-circle mr-10"> Phạm Thị Thơm</a>
                                                    <span class="badge" data-toggle="tooltip" data-placement="top" title=""
                                                        data-original-title="2 lần nhận lớp">2</span></li>
                                                <li><a target="_blank" href="/gia-su/nguyen-tang-ky-15168"><img
                                                            style="width:24px;"
                                                            src="../images/default-avatar.png"
                                                            alt="Nguyễn Tăng Kỳ" class="img-circle mr-10"> Nguyễn Tăng
                                                        Kỳ</a> <span class="badge" data-toggle="tooltip"
                                                        data-placement="top" title=""
                                                        data-original-title="2 lần nhận lớp">2</span></li>
                                                <li><a target="_blank" href="/gia-su/vo-phuc-hanh-38741"><img
                                                            style="width:24px;"
                                                            src="https://www.daykemtainha.vn/public/files/avatar_crop_48_48/38741_avatar.jpeg"
                                                            alt="Võ Phúc Hạnh" class="img-circle mr-10"> Võ Phúc Hạnh</a>
                                                    <span class="badge" data-toggle="tooltip" data-placement="top" title=""
                                                        data-original-title="2 lần nhận lớp">2</span></li>
                                                <li><a target="_blank" href="/gia-su/truong-thi-thu-ha-22117"><img
                                                            style="width:24px;"
                                                            src="https://www.daykemtainha.vn/public/files/avatar_crop_48_48/22117_avatar.jpeg"
                                                            alt="Trương Thị Thu Hà" class="img-circle mr-10"> Trương Thị Thu
                                                        Hà</a> <span class="badge" data-toggle="tooltip"
                                                        data-placement="top" title=""
                                                        data-original-title="1 lần nhận lớp">1</span></li>
                                                <li><a target="_blank" href="/gia-su/huynh-thi-kim-thanh-1550"><img
                                                            style="width:24px;"
                                                            src="https://www.daykemtainha.vn/public/files/avatar_crop_48_48/1550_avatar.jpeg"
                                                            alt="HUỲNH THỊ KIM THANH" class="img-circle mr-10"> HUỲNH THỊ
                                                        KIM THANH</a> <span class="badge" data-toggle="tooltip"
                                                        data-placement="top" title=""
                                                        data-original-title="1 lần nhận lớp">1</span></li>
                                                <li><a target="_blank" href="/gia-su/nguyen-thanh-tung-44502"><img
                                                            style="width:24px;"
                                                            src="https://www.daykemtainha.vn/public/files/avatar_crop_48_48/44502_avatar.jpeg"
                                                            alt="Nguyễn Thanh Tùng" class="img-circle mr-10"> Nguyễn Thanh
                                                        Tùng</a> <span class="badge" data-toggle="tooltip"
                                                        data-placement="top" title=""
                                                        data-original-title="1 lần nhận lớp">1</span></li>
                                                <li><a target="_blank" href="/gia-su/nguyen-hoang-tam-37416"><img
                                                            style="width:24px;"
                                                            src="https://www.daykemtainha.vn/public/files/avatar_crop_48_48/37416_avatar.jpeg"
                                                            alt="Nguyễn Hoàng Tâm" class="img-circle mr-10"> Nguyễn Hoàng
                                                        Tâm</a> <span class="badge" data-toggle="tooltip"
                                                        data-placement="top" title=""
                                                        data-original-title="1 lần nhận lớp">1</span></li>
                                                <li><a target="_blank" href="/gia-su/dinh-vu-uyen-giang-2011"><img
                                                            style="width:24px;"
                                                            src="https://www.daykemtainha.vn/public/files/avatar_crop_48_48/2011_avatar.jpeg"
                                                            alt="Đinh Vũ Uyên Giang" class="img-circle mr-10"> Đinh Vũ Uyên
                                                        Giang</a> <span class="badge" data-toggle="tooltip"
                                                        data-placement="top" title=""
                                                        data-original-title="1 lần nhận lớp">1</span></li>
                                                <li><a target="_blank" href="/gia-su/nguyen-thi-thanh-thuy-43797"><img
                                                            style="width:24px;"
                                                            src="https://www.daykemtainha.vn/public/files/avatar_crop_48_48/43797_avatar.JPG"
                                                            alt="Nguyễn Thị Thanh Thuỷ" class="img-circle mr-10"> Nguyễn Thị
                                                        Thanh Thuỷ</a> <span class="badge" data-toggle="tooltip"
                                                        data-placement="top" title=""
                                                        data-original-title="1 lần nhận lớp">1</span></li>
                                                <li><a target="_blank" href="/gia-su/vu-minh-nhat-43647"><img
                                                            style="width:24px;"
                                                            src="https://www.daykemtainha.vn/public/files/avatar_crop_48_48/43647_avatar.jpeg"
                                                            alt="Vũ Minh Nhật" class="img-circle mr-10"> Vũ Minh Nhật</a>
                                                    <span class="badge" data-toggle="tooltip" data-placement="top" title=""
                                                        data-original-title="1 lần nhận lớp">1</span></li>
    
                                            </ul>
    
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="widget">
                                            <h2 class="widget-title line-bottom-theme-colored-2 ">Lớp <b
                                                    class="text-theme-colored2">Mới</b></h2>
                                            <ul class="angle-double-right list-border">
                                                <li><a target="_blank"
                                                        href="/lop-hoc/can-gia-su-day-kem-tieng-phap-cho-be-lop-4-tai-quan-8-ho-chi-minh-19249"><b>19249</b>
                                                        - Cần gia sư dạy kèm tiếng pháp cho bé lớp 4 tại quận 8, Hồ Chí
                                                        Minh</a> </li>
                                                <li><a target="_blank"
                                                        href="/lop-hoc/can-gia-su-mon-toan-lop-8-tai-quan-8-ho-chi-minh-19248"><b>19248</b>
                                                        - Cần gia sư môn Toán lớp 8 tại quận 8, Hồ Chí Minh</a> </li>
                                                <li><a target="_blank"
                                                        href="/lop-hoc/can-gia-su-mon-nhay-hien-dai-tai-quan-7-ho-chi-minh-19247"><b>19247</b>
                                                        - Cần gia sư môn nhảy hiện đại tại quận 7, Hồ Chí Minh</a> </li>
                                                <li><a target="_blank"
                                                        href="/lop-hoc/can-gia-su-cho-be-hoc-lop-7-tai-bien-hoa-dong-nai-19246"><b>19246</b>
                                                        - Cần gia sư cho bé học lớp 7 tại Biên Hòa, Đồng Nai</a> </li>
                                                <li><a target="_blank"
                                                        href="/lop-hoc/can-1-ban-day-mon-piano-cho-trung-tam-tai-go-vap-ho-chi-minh-19240"><b>19240</b>
                                                        - Cần 1 bạn dạy môn Piano cho trung tâm tại Gò Vấp, Hồ Chí Minh</a>
                                                </li>
                                                <li><a target="_blank"
                                                        href="/lop-hoc/can-gia-su-mon-ukulele-tai-son-tra-da-nang-19227"><b>19227</b>
                                                        - Cần gia sư môn Ukulele tại Sơn Trà , Đà Nẵng</a> </li>
                                                <li><a target="_blank"
                                                        href="/lop-hoc/can-gia-su-mon-tieng-viet-cho-han-quoc-tai-nha-trang-khanh-hoa-hoc-online-19222"><b>19222</b>
                                                        - Cần gia sư môn Tiếng Việt cho Hàn Quốc tại Nha Trang, Khánh Hòa -
                                                        HỌC ONLINE</a> </li>
                                                <li><a target="_blank"
                                                        href="/lop-hoc/can-gia-su-mon-boxing-tai-phu-rieng-binh-phuoc-19214"><b>19214</b>
                                                        - Cần gia sư môn Boxing tại Phú Riềng, Bình Phước</a> </li>
                                                <li><a target="_blank"
                                                        href="/lop-hoc/can-gia-su-mon-trong-jazz-tai-xuyen-moc-ba-ria-vung-tau-19213"><b>19213</b>
                                                        - Cần gia sư môn Trống Jazz tại Xuyên Mộc, Bà Rịa - Vũng Tàu</a>
                                                </li>
                                                <li><a target="_blank"
                                                        href="/lop-hoc/can-gia-su-mon-violin-tai-long-bien-ha-noi-19206"><b>19206</b>
                                                        - Cần gia sư môn Violin tại Long Biên, Hà Nội</a> </li>
    
                                            </ul>
    
                                        </div>
                                    </div>
                                </div>

                                <div class="col-md-12" style="padding: 0;">
                                    <div class="widget">
                                        <h4 class="widget-title line-bottom-theme-colored-2">Tìm gia sư theo các <b
                                                class="text-theme-colored2">môn phổ biến</b> </h4>
                                        <div class="tags subject-tag">

                                            <a class="active" ng-href="/gia-su?mon-hoc=Toán Lớp 2" href="/gia-su?mon-hoc=Toán Lớp 2">Toán Lớp 2</a>
                                            <a
                                                ng-href="/gia-su?mon-hoc=Toán lớp 3" href="/gia-su?mon-hoc=Toán lớp 3">Toán lớp 3</a>
                                                <a
                                                ng-href="/gia-su?mon-hoc=Toán Lớp 1" href="/gia-su?mon-hoc=Toán Lớp 1">Toán Lớp 1</a>
                                                <a
                                                ng-href="/gia-su?mon-hoc=Toán lớp 4" href="/gia-su?mon-hoc=Toán lớp 4">Toán lớp 4</a>
                                                <a
                                                ng-href="/gia-su?mon-hoc=Toán lớp 5" href="/gia-su?mon-hoc=Toán lớp 5">Toán lớp 5</a>
                                                <a
                                                ng-href="/gia-su?mon-hoc=Toán lớp 6" href="/gia-su?mon-hoc=Toán lớp 6">Toán lớp 6</a>
                                                <a
                                                ng-href="/gia-su?mon-hoc=Toán lớp 7" href="/gia-su?mon-hoc=Toán lớp 7">Toán lớp 7</a>
                                                <a
                                                ng-href="/gia-su?mon-hoc=Tiếng Việt Lớp 1" href="/gia-su?mon-hoc=Tiếng Việt Lớp 1">Tiếng Việt Lớp 1</a>
                                                <a
                                                ng-href="/gia-su?mon-hoc=Tiếng Việt Lớp 2" href="/gia-su?mon-hoc=Tiếng Việt Lớp 2">Tiếng Việt Lớp 2</a>
                                                <a
                                                ng-href="/gia-su?mon-hoc=Toán lớp 8" href="/gia-su?mon-hoc=Toán lớp 8">Toán lớp 8</a><a
                                                ng-href="/gia-su?mon-hoc=Tiếng Việt lớp 3" href="/gia-su?mon-hoc=Tiếng Việt lớp 3">Tiếng Việt lớp 3</a>
                                                <a
                                                ng-href="/gia-su?mon-hoc=Tiếng Việt lóp 4" href="/gia-su?mon-hoc=Tiếng Việt lóp 4">Tiếng Việt lóp 4</a>
                                                <a
                                                ng-href="/gia-su?mon-hoc=Tiếng Anh" href="/gia-su?mon-hoc=Tiếng Anh">Tiếng Anh</a>
                                                <a
                                                ng-href="/gia-su?mon-hoc=Tiếng Anh lớp 3" href="/gia-su?mon-hoc=Tiếng Anh lớp 3">Tiếng Anh lớp 3</a>
                                                <a
                                                ng-href="/gia-su?mon-hoc=Toán lớp 9	" href="/gia-su?mon-hoc=Toán lớp 9	">Toán lớp 9 </a>
                                                <a
                                                ng-href="/gia-su?mon-hoc=Đàn Piano" href="/gia-su?mon-hoc=Đàn Piano">Đàn
                                                Piano</a><a ng-href="/gia-su?mon-hoc=Tiếng Anh Lớp 2" href="/gia-su?mon-hoc=Tiếng Anh Lớp 2">Tiếng Anh Lớp 2</a>
                                                <a
                                                ng-href="/gia-su?mon-hoc=Tiếng Anh Lớp 1" href="/gia-su?mon-hoc=Tiếng Anh Lớp 1">Tiếng Anh Lớp 1</a>
                                                <a
                                                ng-href="/gia-su?mon-hoc=Tiếng Việt lớp 5" href="/gia-su?mon-hoc=Tiếng Việt lớp 5">Tiếng Việt lớp 5</a>
                                                <a
                                                ng-href="/gia-su?mon-hoc=Tiếng Anh lóp 4" href="/gia-su?mon-hoc=Tiếng Anh lóp 4">Tiếng Anh lóp 4</a>

                                        </div>

                                    </div>
                                </div>


                            </div>

                        </div>

                    </div>

                </div>

            </div>
        </section>
        <!-- section - event -->
        <section>
            <div class="container pb-50">
              <div class="section-content">
                <div class="row">
                  
                  <div class="col-md-12">
                    <h3 class="text-uppercase line-bottom-theme-colored-2 mt-0 mt-sm-30" style="font-size: 24px; font-family: 'Times New Roman', Times, serif;"><i class="fa fa-question-circle" aria-hidden="true"></i>   Các câu hỏi <span class="text-success">thường gặp</span> của Gia Sư</h3>
                    <div class="panel-group accordion-stylished-left-border accordion-icon-filled accordion-no-border accordion-icon-left accordion-icon-filled-theme-colored2 pt-4" id="accordion6" role="tablist" aria-multiselectable="true">
                      <div class="panel panel-default">
                        <div class="panel-heading" role="tab" id="headin1">
                          <h6 class="panel-title" >
                            <a role="button" data-toggle="collapse" data-parent="#accordion6" href="#collaps1" aria-expanded="true" aria-controls="collaps1">
                                <i class="fa fa-plus-square" aria-hidden="true"></i>
                              Quy trình nhận lớp thế nào?
                            </a>
                          </h6>
                        </div>
                        <div id="collaps1" class="panel-collapse collapse in" role="tabpanel" aria-labelledby="headin1">
                          <div class="panel-body">
                            <div class="row">
                              <div class="col-md-4">
                                <b class="text-theme-colored2-title">Bước 1:</b> <br>
                                  - Vào website Daykemtainha.vn  <br>
                                  - Chọn mục:Đăng ký làm gia sư <br>
                                  - Điền và chọn đầy đủ thông tin (chú ý các vị trí có dấu * phải có đầy đủ, trung tâm ưu tiên các bạn có thông tin tốt và có cmnd bằng cấp đầy đủ)<br>
                                  - Chọn nút “Đăng ký làm gia sư”<br>
                                  - Sau khi đăng ký gia sư thành công bạn chọn đăng nhập bằng mật khẩu hoặc facebook hoặc google<br>
                              </div>
                              <div class="col-md-4">
                                <b class="text-theme-colored2">Bước 2:</b><br> 
                                  - Vào website: Daykemtainha.vn<br>
                                  - Chọn mục:Lớp mới <br>
                                  - Hãy tìm đến lớp mà bạn muốn nhận (có thể tìm kiếm theo môn học để tìm nhanh hơn)<br>
                                  - Nhấn vào nút: “Nhận lớp ngay” và đợi trung tâm phản hồi.<br>
                              </div>
                              <div class="col-md-4">
                                <b class="text-theme-colored2">Bước 3:</b> <br>
                                  Sau khi bạn nhận lớp thành công<br>
                                  - Vào website: Daykemtainha.vn (chú ý là bạn đã đăng nhập rồi nhé)<br>
                                  - Bạn vào profile của bạn chọn mục: danh sách lớp dạy <br>
                                  - Tìm đến lớp mà bạn muốn xem <br>
                                  - Nhấn vào nút: Chi tiết <br>
                                  - Hãy liên hệ với phụ huynh học sinh liền nhé (nếu bạn nhận được lớp<br>
                              </div>
                          </div>
                        </div>
                      </div>
               
                      <div class="panel panel-default">
                        <div class="panel-heading" role="tab" id="heading3">
                          <h6 class="panel-title">
                            <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion6" href="#collapse3" aria-expanded="false" aria-controls="collapse3">
                                <i class="fa fa-plus-square" aria-hidden="true"></i>
                              Số tài khoản của trung tâm
                            </a>
                          </h6>
                        </div>
                        <div id="collapse3" class="panel-collapse collapse" role="tabpanel" aria-labelledby="heading3">
                          <div class="panel-body">
                            Sau khi thống nhất nhận lớp với trung tâm, gia sư chuyển khoản 1 trong các tài khoản dưới đây: <br>
                            <div class="row">
                              <div class="col-md-4">
                                <b class="text-theme-colored2">Thông tin tài khoản 1</b><br>
                                Ngân hàng: &nbsp;Đông Á chi nhánh An Đông Quận 5<br>
                                Chủ tài khoản: Nguyễn Thị Mượt<br>
                                Số tài khoản: 010 77 034 64<br>
                                <hr>
                              </div>
                              <div class="col-md-4">
                                <b class="text-theme-colored2">Thông tin tài khoản 2</b><br>
                                Ngân hàng: &nbsp;Vietcombank chi nhánh nam sài gòn&nbsp;<br>
                                Chủ tài khoản: Nguyễn Thị Mượt<br>
                                Số tài khoản: 0181 003 452 361&nbsp;<br>
                                <hr>
                              </div>
                              <div class="col-md-4">
                                <b class="text-theme-colored2">Thông tin tài khoản 3 &nbsp;</b><br>
                                Ngân hàng VietinBank &nbsp;chi nhánh quận 8<br>
                                Chủ tài khoản: Nguyễn Thị Mượt &nbsp;<br>
                                Số tài khoản:&nbsp;100005833143<br>
                                <hr>
                              </div>
                              <div class="col-md-4">
                                <b class="text-theme-colored2">Thông tin tài khoản 4 &nbsp;</b><br>
                                Ngân hàng Sacombank &nbsp;chi nhánh quận 8<br>
                                Chủ tài khoản: Nguyễn Thị Mượt &nbsp;<br>
                                Số tài khoản:&nbsp;060142278874<br>
                                <hr>
                              </div>
                              <div class="col-md-4">
                                <b class="text-theme-colored2">Thông tin tài khoản 5</b><br>
                                Ngân hàng&nbsp;Agribank&nbsp;chi nhanh Quận 8<br>
                                Chủ tài khoản: Nguyễn Thị Mượt<br>
                                Số tài khoản:&nbsp;1702 205 251 209&nbsp;<br>
                                <hr>
                              </div>
                              <div class="col-md-4">
                                <b class="text-theme-colored2">Thông tin tài khoản 6</b><br>
                                Ngân hàng ACB chi nhánh Tùng Thiện Vương<br>
                                Chủ tài khoản: Nguyễn Thị Mượt<br>
                                Số tài khoản:&nbsp;247 44 7219<br>
                                <hr>
                              </div>
                              <div class="col-md-4">
                                <b class="text-theme-colored2">Thông tin tài khoản 7</b><br>
                                Ngân hàng BIDV chi nhánh Bình Chánh<br>
                                Chủ tài khoản: Nguyễn Thị Mượt<br>
                                Số tài khoản:&nbsp;3171 00000 877 37&nbsp;<br>
                                <hr>
                              </div>
                              <div class="col-md-4">
                                <b class="text-theme-colored2">Thông tin tài khoản 8</b><br>
                                Ngân hàng VP Bank, chi nhánh Quận 8<br>
                                Chủ tài khoản: Nguyễn Thị Mượt<br>
                                Số tài khoản:&nbsp;142 119 129&nbsp;<br>
                                <hr>
                              </div>
                              <div class="col-md-4">
                                <b class="text-theme-colored2">Thông tin tài khoản 9</b><br>
                                Ngân hàng TECHCOMBANK chi nhánh Thuận Kiều<br>
                                Chủ tài khoản: Nguyễn Thị Mượt<br>
                                Số tài khoản:&nbsp;1903 290 1455 015<br>
                                <hr>
                              </div>
                          </div>
                        </div>
                      </div>
                      <div class="panel panel-default">
                        <div class="panel-heading" role="tab" id="heading4">
                          <h6 class="panel-title">
                            <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion6" href="#collapse4" aria-expanded="false" aria-controls="collapse4">
                                <i class="fa fa-plus-square" aria-hidden="true"></i>
                              Số điện thoại hỗ trợ 24/7
                            </a>
                          </h6>
                        </div>
                        <div id="collapse4" class="panel-collapse collapse" role="tabpanel" aria-labelledby="heading4">
                          <div class="panel-body">
                            Hãy gọi ngay  <a href="tel:0378793050" class="text-theme-colored2">037 879 3050</a> hoặc <a href="tel:0378793050" class="text-theme-colored2">037 879 3050</a>  để được hỗ trợ?
                          </div>
                        </div>
                      </div>
                      
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div></div></section>
    </div>
    <jsp:include page="footer.jsp"></jsp:include>
    <!-- Optional JavaScript -->
    <!-- jQuery first, then Popper.js, then Bootstrap JS -->
    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js"
        integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo"
        crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js"
        integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1"
        crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"
        integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM"
        crossorigin="anonymous"></script>
</body>

</html>