<%-- 
    Document   : viewskill
    Created on : May 29, 2023, 10:47:41 AM
    Author     : quanr
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!doctype html>
<html lang="en">
  <head>
    <title>Title</title>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
    <link rel="stylesheet" href="./css/viewskill.css">

</head>
  <body>
    <!doctype html>
    <html lang="en">
    
    <head>
        <title>Title</title>
        <!-- Required meta tags -->
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    
        <!-- Bootstrap CSS -->
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css"
            integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
        <link rel="stylesheet" href="../css/viewprofilementor.css">
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
    </head>
    
    <body>
        <jsp:include page="menu.jsp" ></jsp:include>
        <div ng-app="mainApp" ng-controller="mainCtroller" class="ng-scope pt-5">
            <section class="teacher-prolile-01">
                <div class="container pb-0">
                    <div class="row">
                        <div class="col-sm-3 t-profile-left teacher-item" id="teacher_45122">
                            <div class="teacher-contact">
                                <img src="https://www.daykemtainha.vn/public/files/avatar_crop/45122_avatar.jpeg" 
                                    alt="Nguyễn Huỳnh Yến Nhi" class="img-responsive">
                                <div class="stars-vote" style="display:none;">
                                    <i class="fas fa-star"></i>
                                    <i class="fas fa-star"></i>
                                    <i class="fas fa-star"></i>
                                    <i class="fas fa-star"></i>
                                    <i class="fas fa-star-half-alt"></i>
                                </div>
    
                            </div>
                            <hr>
                            <div class="tao-yeu-cau ">
                                <div class="buttonCtrl " style="text-align: center;">
                                    <a class=" btn btn-success btn-create-class" ng-href="/tim-gia-su" href="/tim-gia-su">Join now</a>
    
                                </div>
    
                            </div>
    
                        </div>
    
                        <div class="col-sm-9 t-profile-right">
                            <div class="teacher01-content">
                                <div class="row all-corsess-wapper">
                                     <c:forEach items="${listS}" var="o">
                                    <div class="col-sm-7">
                                        
                                        <div class="all-courses">
                                            <h3>${listS.skill_name}</h3>
                                            <div class="profile__courses__inner d-flex" style="column-gap: 10px;">
                                                <ul class="profile__courses__list list-unstyled">
                                                    <li><i class="fa fa-bookmark"></i> Decription:</li>
                                                    <li><i class="fa fa-map-marker" aria-hidden="true"></i> Giá:</li>
                                                </ul>
                                                <ul class="profile__courses__right list-unstyled">
                                                    <li>${listS.decription}</li>
                                                    <li>${listS.skill_price}</li>
                                                </ul>
                                            </div>
                                        </div>
                                    </div>
                                        </c:forEach>
                                    <div class="col-md-5">
                                        <div class="widget">
                                            <h4 class="widget-title line-bottom-theme-colored-2">Các gia sư <b>môn học</b>
                        
                                            </h4>
                        
                                            <ul class="angle-double-right list-border p-0" style="list-style: none;">
                                              <li><a target="_blank" href="/thong-tin-gia-su/pham-thi-thom-2946"><img style="width:24px;"
                                                    src="https://www.daykemtainha.vn/public/files/avatar_crop_48_48/2946_avatar.jpg"
                                                    alt="Phạm Thị Thơm" class="img-circle mr-10"> Phạm Thị Thơm</a> </li>
                                              <li><a target="_blank" href="/thong-tin-gia-su/nguyen-tang-ky-15168"><img style="width:24px;"
                                                    src="https://www.daykemtainha.vn/public/files/avatar_crop_48_48/15168_avatar.jpg"
                                                    alt="Nguyễn Tăng Kỳ" class="img-circle mr-10"> Nguyễn Tăng Kỳ</a> </li>
                                              <li><a target="_blank" href="/thong-tin-gia-su/nguyen-thi-bao-yen-44472"><img style="width:24px;"
                                                    src="https://www.daykemtainha.vn/public/files/avatar_crop_48_48/44472_avatar.jpg"
                                                    alt="Nguyễn Thị Bảo Yến" class="img-circle mr-10"> Nguyễn Thị Bảo Yến</a> </li>
                                              <li><a target="_blank" href="/thong-tin-gia-su/pham-thi-huynh-dat-4911"><img style="width:24px;"
                                                    src="https://www.daykemtainha.vn/public/files/avatar_crop_48_48/4911_avatar.jpeg"
                                                    alt="Phạm Thị Huỳnh Đạt" class="img-circle mr-10"> Phạm Thị Huỳnh Đạt</a> </li>
                                              <li><a target="_blank" href="/thong-tin-gia-su/vo-phuc-hanh-38741"><img style="width:24px;"
                                                    src="https://www.daykemtainha.vn/public/files/avatar_crop_48_48/38741_avatar.jpeg"
                                                    alt="Võ Phúc Hạnh" class="img-circle mr-10"> Võ Phúc Hạnh</a> </li>
                                              <li><a target="_blank" href="/thong-tin-gia-su/truong-thi-thu-ha-22117"><img style="width:24px;"
                                                    src="https://www.daykemtainha.vn/public/files/avatar_crop_48_48/22117_avatar.jpeg"
                                                    alt="Trương Thị Thu Hà" class="img-circle mr-10"> Trương Thị Thu Hà</a> </li>
                                              <li><a target="_blank" href="/thong-tin-gia-su/huynh-thi-kim-thanh-1550"><img style="width:24px;"
                                                    src="https://www.daykemtainha.vn/public/files/avatar_crop_48_48/1550_avatar.jpeg"
                                                    alt="HUỲNH THỊ KIM THANH" class="img-circle mr-10"> HUỲNH THỊ KIM THANH</a> </li>
                                              <li><a target="_blank" href="/thong-tin-gia-su/nguyen-thanh-tung-44502"><img style="width:24px;"
                                                    src="https://www.daykemtainha.vn/public/files/avatar_crop_48_48/44502_avatar.jpeg"
                                                    alt="Nguyễn Thanh Tùng" class="img-circle mr-10"> Nguyễn Thanh Tùng</a> </li>
                                              <li><a target="_blank" href="/thong-tin-gia-su/nguyen-hoang-tam-37416"><img style="width:24px;"
                                                    src="https://www.daykemtainha.vn/public/files/avatar_crop_48_48/37416_avatar.jpeg"
                                                    alt="Nguyễn Hoàng Tâm" class="img-circle mr-10"> Nguyễn Hoàng Tâm</a></li>
                                              <li><a target="_blank" href="/thong-tin-gia-su/nguyen-thi-thao-nguyen-756"><img
                                                    style="width:24px;"
                                                    src="https://www.daykemtainha.vn/public/files/avatar_crop_48_48/756_avatar.jpg"
                                                    alt="nguyễn thị thảo nguyên" class="img-circle mr-10"> nguyễn thị thảo nguyên</a> </li>
                        
                                            </ul>
                        
                                          </div>
                                    </div>
                                </div>
    
    
                            </div>
                           
    
                        </div>
                    </div>
                </div>
            </section>
    
    
        </div>
          <jsp:include page="footer.jsp" ></jsp:include>
        <!-- Optional JavaScript -->
        <!-- jQuery first, then Popper.js, then Bootstrap JS -->
        <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js"
            integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo"
            crossorigin="anonymous"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js"
            integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1"
            crossorigin="anonymous"></script>
        <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"
            integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM"
            crossorigin="anonymous"></script>
    </body>
    
    </html>
    <!-- Optional JavaScript -->
    <!-- jQuery first, then Popper.js, then Bootstrap JS -->
    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>
  </body>
</html>
