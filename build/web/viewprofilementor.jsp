

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!doctype html>
<html lang="en">

<head>
    <title>Title</title>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css"
        integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
    <link rel="stylesheet" href="./css/viewprofilementor.css">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
</head>

<body>
    <div ng-app="mainApp" ng-controller="mainCtroller" class="ng-scope pt-5">
        <section class="teacher-prolile-01">
            <div class="container pb-0">
                <div class="row">
                    <div class="col-sm-3 t-profile-left teacher-item" id="teacher_45122">
                        <div class="teacher-contact">
                            <img src="https://www.daykemtainha.vn/public/files/avatar_crop/45122_avatar.jpeg"
                                alt="Nguyễn Huỳnh Yến Nhi" class="img-responsive">
                            <div class="stars-vote" style="display:none;">
                                <i class="fas fa-star"></i>
                                <i class="fas fa-star"></i>
                                <i class="fas fa-star"></i>
                                <i class="fas fa-star"></i>
                                <i class="fas fa-star-half-alt"></i>
                            </div>

                        </div>
                        <hr>
                        <div class="tao-yeu-cau ">
                            <div class="buttonCtrl " style="text-align: center;">
                                <a class=" btn btn-success btn-create-class" ng-href="/tim-gia-su" href="/tim-gia-su">Tạo
                                    yêu cầu</a>

                            </div>

                        </div>

                    </div>

                    <div class="col-sm-9 t-profile-right">
                        <div class="teacher01-content">
                            <div class="row all-corsess-wapper">
                                <div class="col-sm-12">
                                    <div class="all-courses">
                                        <h3>Nguyễn Huỳnh Yến Nhi</h3>
                                        <div class="profile__courses__inner d-flex" style="column-gap: 10px;">
                                            <ul class="profile__courses__list list-unstyled">
                                                <li><i class="fa fa-bookmark"></i> Năm sinh:</li>
                                                <li><i class="fa fa-graduation-cap"></i> Trình Độ:</li>

                                                <li><i class="fa fa-map-marker" aria-hidden="true"></i> Nơi ở:</li>
                                                <li><i class="fa fa-book" aria-hidden="true"></i> Môn dạy:</li>
                                                <li><i class="fa fa-map-marker" aria-hidden="true"></i> Có thể dạy tại:</li>
                                                <!-- <li><i class="far fa-calendar-alt"></i>Thời gian dạy:</li> -->



                                            </ul>
                                            <ul class="profile__courses__right list-unstyled">
                                                <li>2003</li>
                                                <li>Sinh viên</li>

                                                <li>Hồ Chí Minh</li>

                                                <li>Nhảy hiện đại (Dance), Nhảy Zumba</li>
                                                <li>Quận 10, Huyện Bình Chánh</li>
                                                <!-- <li>
                                                <li>Thứ 2, Thứ 4, Thứ 5, Thứ 6, Thứ 7: <span> 1h - 6h,  13h - 16h,  22h - 23h</span></li><li>Thứ 3, Chủ nhật: <span> 0h - 6h,  13h - 16h,  22h - 23h</span></li>                                            </li> -->

                                            </ul>
                                        </div>
                                        <p style="color: #333;
                                        font-size: 15px;">Mình có năng khiếu nhảy, đã từng hướng dẫn nhảy, biên đạo, tham gia nhiều
                                            hoạt động nhảy. Mình thân thiện, yêu trẻ nít, nhẫn nhịn, cầu tiến.</p>
                                    </div>
                                </div>
                            </div>


                        </div>
                        <!-- ngIf: classes -->
                        <div class="teacher01-content mt-20 mb-50 ng-scope" ng-if="classes">
                            <div class="row all-corsess-wapper">
                                <div class="col-sm-12">
                                    <div class="all-courses">

                                        <!-- ngIf: classes.length>0 -->
                                        <ul class="classes-success">
                                            <!-- ngRepeat: class in classes -->
                                        </ul>
                                        <!-- <center>
                                            <button class="btn btn-info">Xem thêm</button>
                                        </center> -->
                                    </div>
                                </div>
                            </div>
                        </div><!-- end ngIf: classes -->

                    </div>
                </div>
            </div>
        </section>


    </div>
    <!-- Optional JavaScript -->
    <!-- jQuery first, then Popper.js, then Bootstrap JS -->
    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js"
        integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo"
        crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js"
        integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1"
        crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"
        integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM"
        crossorigin="anonymous"></script>
</body>

</html>
